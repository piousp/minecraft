REIEvents.hide("item", event => {
    event.hide("ad_astra:fuel_refinery");
    event.hide("ad_astra:hammer");
    event.hide("ad_astra:compressor");
    event.hide("ad_astra:cryo_freezer");
});