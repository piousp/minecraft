// priority: 0

REIEvents.hide('item', event => {
    // máquinas
    event.hide("create:millstone");
    event.hide("create:mechanical_press");
    event.hide("create:mechanical_mixer");
    event.hide("create:crushing_wheel");
    event.hide("create:mechanical_crafter");
    
    // como el mixer se eliminó este item ya no tiene uso
    event.hide("create:whisk");
    
    // crushed ores
    event.hide("create:crushed_zinc_ore");
    event.hide("create:crushed_lead_ore");
    event.hide("create:crushed_copper_ore");
    event.hide("create:crushed_nickel_ore");
    event.hide("create:crushed_silver_ore");
    event.hide("create:crushed_iron_ore");
    event.hide("create:crushed_gold_ore");
    event.hide("create:crushed_uranium_ore");
    event.hide("create:crushed_platinum_ore");
    event.hide("create:crushed_tin_ore");
})
