//https://minecraft.fandom.com/wiki/Tiers
//https://minecraft.fandom.com/wiki/Armor_materials
ItemEvents.toolTierRegistry (event => {
  event.add("ostrum", tier => {
    tier.uses = 500
    tier.speed = 20.0
    tier.attackDamageBonus = 2.0
    tier.level = 3
    tier.enchantmentValue = 33
    tier.repairIngredient = "#c:ostrum_ingots"
  });
});

ItemEvents.armorTierRegistry (event => {
  event.add("ostrum", tier => {
    tier.durabilityMultiplier = 20 // Each slot will be multiplied with [13, 15, 16, 11]
    tier.slotProtections = [3, 7, 6, 3] // Slot indicies are [FEET, LEGS, BODY, HEAD] 
    tier.enchantmentValue = 33
    tier.equipSound = "minecraft:item.armor.equip_iron"
    tier.repairIngredient = "#c:ostrum_ingots"
    tier.toughness = 1.0 // diamond has 2.0, netherite 3.0
    tier.knockbackResistance = 0.0
  });
});

StartupEvents.registry("item", event => {
  event.create("ostrum_axe", "axe").tier("ostrum");
  event.create("ostrum_pickaxe", "pickaxe").tier("ostrum");
  event.create("ostrum_sword", "sword").tier("ostrum");
  event.create("ostrum_shovel", "shovel").tier("ostrum");
  event.create("ostrum_hoe", "hoe").tier("ostrum");

  event.create("ostrum_helmet", "helmet").tier("ostrum");
  event.create("ostrum_chestplate", "chestplate").tier("ostrum");
  event.create("ostrum_leggings", "leggings").tier("ostrum");
  event.create("ostrum_boots", "boots").tier("ostrum");
});
