//https://minecraft.fandom.com/wiki/Tiers
//https://minecraft.fandom.com/wiki/Armor_materials
ItemEvents.toolTierRegistry (event => {
  event.add("bronze", tier => {
    tier.uses = 250
    tier.speed = 5.0
    tier.attackDamageBonus = 2.0
    tier.level = 2
    tier.enchantmentValue = 15
    tier.repairIngredient = "#c:bronze_ingots"
  });
});

ItemEvents.armorTierRegistry (event => {
  event.add("bronze", tier => {
    tier.durabilityMultiplier = 13 // Each slot will be multiplied with [13, 15, 16, 11]
    tier.slotProtections = [2, 4, 5, 2] // Slot indicies are [FEET, LEGS, BODY, HEAD] 
    tier.enchantmentValue = 11
    tier.equipSound = "minecraft:item.armor.equip_iron"
    tier.repairIngredient = "#c:bronze_ingots"
    tier.toughness = 0.0 // diamond has 2.0, netherite 3.0
    tier.knockbackResistance = 0.0
  });
});

StartupEvents.registry("item", event => {
  event.create("bronze_axe", "axe").tier("bronze");
  event.create("bronze_pickaxe", "pickaxe").tier("bronze");
  event.create("bronze_sword", "sword").tier("bronze");
  event.create("bronze_shovel", "shovel").tier("bronze");
  event.create("bronze_hoe", "hoe").tier("bronze");

  event.create("bronze_helmet", "helmet").tier("bronze");
  event.create("bronze_chestplate", "chestplate").tier("bronze");
  event.create("bronze_leggings", "leggings").tier("bronze");
  event.create("bronze_boots", "boots").tier("bronze");
});
