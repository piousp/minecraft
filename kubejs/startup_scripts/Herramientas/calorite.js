//https://minecraft.fandom.com/wiki/Tiers
//https://minecraft.fandom.com/wiki/Armor_materials
ItemEvents.toolTierRegistry (event => {
  event.add("calorite", tier => {
    tier.uses = 1000
    tier.speed = 25.0
    tier.attackDamageBonus = 3.0
    tier.level = 4
    tier.enchantmentValue = 39
    tier.repairIngredient = "#c:calorite_ingots"
  });
});

ItemEvents.armorTierRegistry (event => {
  event.add("calorite", tier => {
    tier.durabilityMultiplier = 24 // Each slot will be multiplied with [13, 15, 16, 11]
    tier.slotProtections = [3, 7, 6, 3] // Slot indicies are [FEET, LEGS, BODY, HEAD] 
    tier.enchantmentValue = 39
    tier.equipSound = "minecraft:item.armor.equip_iron"
    tier.repairIngredient = "#c:calorite_ingots"
    tier.toughness = 3.0 // diamond has 2.0, netherite 3.0
    tier.knockbackResistance = 0.5
  });
});

StartupEvents.registry("item", event => {
  event.create("calorite_axe", "axe").tier("calorite");
  event.create("calorite_pickaxe", "pickaxe").tier("calorite");
  event.create("calorite_sword", "sword").tier("calorite");
  event.create("calorite_shovel", "shovel").tier("calorite");
  event.create("calorite_hoe", "hoe").tier("calorite");

  event.create("calorite_helmet", "helmet").tier("calorite");
  event.create("calorite_chestplate", "chestplate").tier("calorite");
  event.create("calorite_leggings", "leggings").tier("calorite");
  event.create("calorite_boots", "boots").tier("calorite");
});
