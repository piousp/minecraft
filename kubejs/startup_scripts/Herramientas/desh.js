//https://minecraft.fandom.com/wiki/Tiers
//https://minecraft.fandom.com/wiki/Armor_materials
ItemEvents.toolTierRegistry (event => {
  event.add("desh", tier => {
    tier.uses = 131
    tier.speed = 15.0
    tier.attackDamageBonus = 1.0
    tier.level = 2
    tier.enchantmentValue = 28
    tier.repairIngredient = "#c:desh_ingots"
  });
});

ItemEvents.armorTierRegistry (event => {
  event.add("desh", tier => {
    tier.durabilityMultiplier = 15 // Each slot will be multiplied with [13, 15, 16, 11]
    tier.slotProtections = [2, 6, 5, 2] // Slot indicies are [FEET, LEGS, BODY, HEAD] 
    tier.enchantmentValue = 28
    tier.equipSound = "minecraft:item.armor.equip_iron"
    tier.repairIngredient = "#c:desh_ingots"
    tier.toughness = 0.0 // diamond has 2.0, netherite 3.0
    tier.knockbackResistance = 0.0
  });
});

StartupEvents.registry("item", event => {
  event.create("desh_axe", "axe").tier("desh");
  event.create("desh_pickaxe", "pickaxe").tier("desh");
  event.create("desh_sword", "sword").tier("desh");
  event.create("desh_shovel", "shovel").tier("desh");
  event.create("desh_hoe", "hoe").tier("desh");

  event.create("desh_helmet", "helmet").tier("desh");
  event.create("desh_chestplate", "chestplate").tier("desh");
  event.create("desh_leggings", "leggings").tier("desh");
  event.create("desh_boots", "boots").tier("desh");
});
