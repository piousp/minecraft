//https://minecraft.fandom.com/wiki/Tiers
//https://minecraft.fandom.com/wiki/Armor_materials
ItemEvents.toolTierRegistry (event => {
  event.add("titanium", tier => {
    tier.uses = 1670
    tier.speed = 8.0
    tier.attackDamageBonus = 2.0
    tier.level = 4
    tier.enchantmentValue = 10
    tier.repairIngredient = "#c:titanium_ingots"
  });
});

ItemEvents.armorTierRegistry (event => {
  event.add("titanium", tier => {
    tier.durabilityMultiplier = 35 // Each slot will be multiplied with [13, 15, 16, 11]
    tier.slotProtections = [3, 6, 8, 3] // Slot indicies are [FEET, LEGS, BODY, HEAD] 
    tier.enchantmentValue = 8
    tier.equipSound = "minecraft:item.armor.equip_netherite"
    tier.repairIngredient = "#c:titanium_ingots"
    tier.toughness = 2.0 // diamond has 2.0, netherite 3.0
    tier.knockbackResistance = 0.1
  });
});

StartupEvents.registry("item", event => {
  event.create("titanium_axe", "axe").tier("titanium");
  event.create("titanium_pickaxe", "pickaxe").tier("titanium");
  event.create("titanium_sword", "sword").tier("titanium");
  event.create("titanium_shovel", "shovel").tier("titanium");
  event.create("titanium_hoe", "hoe").tier("titanium");

  event.create("titanium_helmet", "helmet").tier("titanium");
  event.create("titanium_chestplate", "chestplate").tier("titanium");
  event.create("titanium_leggings", "leggings").tier("titanium");
  event.create("titanium_boots", "boots").tier("titanium");
});
