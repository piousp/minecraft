//https://minecraft.fandom.com/wiki/Tiers
//https://minecraft.fandom.com/wiki/Armor_materials
ItemEvents.toolTierRegistry (event => {
  event.add("copper", tier => {
    tier.uses = 200
    tier.speed = 4.0
    tier.attackDamageBonus = 2.0
    tier.level = 2
    tier.enchantmentValue = 16
    tier.repairIngredient = "#c:copper_ingots"
  });
});

ItemEvents.armorTierRegistry (event => {
  event.add("copper", tier => {
    tier.durabilityMultiplier = 10 // Each slot will be multiplied with [13, 15, 16, 11]
    tier.slotProtections = [1, 3, 5, 1] // Slot indicies are [FEET, LEGS, BODY, HEAD] 
    tier.enchantmentValue = 10
    tier.equipSound = "minecraft:item.armor.equip_iron"
    tier.repairIngredient = "#c:copper_ingots"
    tier.toughness = 0.0 // diamond has 2.0, netherite 3.0
    tier.knockbackResistance = 0.0
  });
});

StartupEvents.registry("item", event => {
  event.create("copper_axe", "axe").tier("copper");
  event.create("copper_pickaxe", "pickaxe").tier("copper");
  event.create("copper_sword", "sword").tier("copper");
  event.create("copper_shovel", "shovel").tier("copper");
  event.create("copper_hoe", "hoe").tier("copper");

  event.create("copper_helmet", "helmet").tier("copper");
  event.create("copper_chestplate", "chestplate").tier("copper");
  event.create("copper_leggings", "leggings").tier("copper");
  event.create("copper_boots", "boots").tier("copper");
});
