//https://minecraft.fandom.com/wiki/Tiers
//https://minecraft.fandom.com/wiki/Armor_materials
ItemEvents.toolTierRegistry (event => {
  event.add("steel", tier => {
    tier.uses = 770
    tier.speed = 6.0
    tier.attackDamageBonus = 2.0
    tier.level = 3
    tier.enchantmentValue = 11
    tier.repairIngredient = "#c:steel_ingots"
  });
});

ItemEvents.armorTierRegistry (event => {
  event.add("steel", tier => {
    tier.durabilityMultiplier = 25 // Each slot will be multiplied with [13, 15, 16, 11]
    tier.slotProtections = [3, 5, 7, 2] // Slot indicies are [FEET, LEGS, BODY, HEAD] 
    tier.enchantmentValue = 8
    tier.equipSound = "minecraft:item.armor.equip_diamond"
    tier.repairIngredient = "#c:steel_ingots"
    tier.toughness = 1.0 // diamond has 2.0, netherite 3.0
    tier.knockbackResistance = 0.0
  });
});

StartupEvents.registry("item", event => {
  event.create("steel_axe", "axe").tier("steel");
  event.create("steel_pickaxe", "pickaxe").tier("steel");
  event.create("steel_sword", "sword").tier("steel");
  event.create("steel_shovel", "shovel").tier("steel");
  event.create("steel_hoe", "hoe").tier("steel");

  event.create("steel_helmet", "helmet").tier("steel");
  event.create("steel_chestplate", "chestplate").tier("steel");
  event.create("steel_leggings", "leggings").tier("steel");
  event.create("steel_boots", "boots").tier("steel");
});
