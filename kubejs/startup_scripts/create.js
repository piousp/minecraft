StartupEvents.registry('item', event => {
    // brass
    event.create('brass_dust').displayName('Brass Dust');
    event.create('brass_tiny_dust').displayName('Brass Tiny Dust');
    
    // zinc
    event.create("raw_zinc").displayName("Raw Zinc");
    event.create("zinc_dust").displayName("Zinc Dust");
    event.create("zinc_tiny_dust").displayName("Zinc Tiny Dust");
    event.create("zinc_ingot").displayName("Zinc Ingot");
    event.create("zinc_nugget").displayName("Zinc Nugget");
})