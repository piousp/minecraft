ServerEvents.recipes(event => {
    event.custom({
        "type": "create:splashing",
        "ingredients": [{
            "tag": "c:sand"
        }],
        "results": [{
            "item": "minecraft:clay"
        }]
    });
})