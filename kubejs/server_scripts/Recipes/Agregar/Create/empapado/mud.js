ServerEvents.recipes(event => {
    event.custom({
        "type": "create:splashing",
        "ingredients": [{
            "item": "minecraft:dirt"
        }],
        "results": [{
            "item": "minecraft:mud"
        }]
    });
})