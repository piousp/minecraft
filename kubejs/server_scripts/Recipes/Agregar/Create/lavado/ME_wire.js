ServerEvents.recipes(event => {
    event.custom({
        "type": "create:splashing",
        "ingredients": [{
            "tag": "modern_industrialization:me_wires"
        }],
        "results": [{
            "item": "modern_industrialization:me_wire"
        }]
    });
})