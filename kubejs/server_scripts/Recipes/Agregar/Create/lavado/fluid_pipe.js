ServerEvents.recipes(event => {
    event.custom({
        "type": "create:splashing",
        "ingredients": [{
            "tag": "modern_industrialization:fluid_pipes"
        }],
        "results": [{
            "item": "modern_industrialization:fluid_pipe"
        }]
    });
})