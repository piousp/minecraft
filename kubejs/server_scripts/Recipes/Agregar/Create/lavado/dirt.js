ServerEvents.recipes(event => {
    event.custom({
        "type": "create:splashing",
        "ingredients": [{
            "item": "minecraft:coarse_dirt"
        }],
        "results": [{
            "item": "minecraft:dirt"
        }, {
            "item": "minecraft:flint",
            "chance": 0.5
        }]
    });
})