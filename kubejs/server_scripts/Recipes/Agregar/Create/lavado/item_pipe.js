ServerEvents.recipes(event => {
    event.custom({
        "type": "create:splashing",
        "ingredients": [{
            "tag": "modern_industrialization:item_pipes"
        }],
        "results": [{
            "item": "modern_industrialization:item_pipe"
        }]
    });
})