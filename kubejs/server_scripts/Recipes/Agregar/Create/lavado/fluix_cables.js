ServerEvents.recipes(event => {
    // glass cable
    event.custom({
        "type": "create:splashing",
        "ingredients": [{
            "tag": "ae2:glass_cable"
        }],
        "results": [{
            "item": "ae2:fluix_glass_cable"
        }]
    });
    
    // smart cable
    event.custom({
        "type": "create:splashing",
        "ingredients": [{
            "tag": "ae2:smart_cable"
        }],
        "results": [{
            "item": "ae2:fluix_smart_cable"
        }]
    });
    
    // dense smart cable
    event.custom({
        "type": "create:splashing",
        "ingredients": [{
            "tag": "ae2:smart_dense_cable"
        }],
        "results": [{
            "item": "ae2:fluix_smart_dense_cable"
        }]
    });
    
    // covered smart cable
    event.custom({
        "type": "create:splashing",
        "ingredients": [{
            "tag": "ae2:covered_cable"
        }],
        "results": [{
            "item": "ae2:fluix_covered_cable"
        }]
    });
    
    // dense covered smart cable
    event.custom({
        "type": "create:splashing",
        "ingredients": [{
            "tag": "ae2:covered_dense_cable"
        }],
        "results": [{
            "item": "ae2:fluix_covered_dense_cable"
        }]
    });
})