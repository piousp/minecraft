ServerEvents.recipes(event => {
    event.custom({
        "type": "create:splashing",
        "ingredients": [{
            "tag": "ae2:memory_cards"
        }],
        "results": [{
            "item": "ae2:memory_card"
        }]
    });
})