ServerEvents.recipes(event => {
  
  event.shaped("kubejs:ostrum_pickaxe", [
    "CCC",
    " S ",
    " S "
  ], {
    C: "#c:ostrum_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:ostrum_axe", [
    "CC ",
    "CS ",
    " S "
  ], {
    C: "#c:ostrum_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:ostrum_hoe", [
    "CC ",
    " S ",
    " S "
  ], {
    C: "#c:ostrum_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:ostrum_sword", [
    " C ",
    " C ",
    " S "
  ], {
    C: "#c:ostrum_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:ostrum_shovel", [
    " C ",
    " S ",
    " S "
  ], {
    C: "#c:ostrum_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:ostrum_helmet", [
    "CCC",
    "C C",
    "   "
  ], {
    C: "#c:ostrum_ingots"
  });

  event.shaped("kubejs:ostrum_boots", [
    "C C",
    "C C",
    "   "
  ], {
    C: "#c:ostrum_ingots"
  });

  event.shaped("kubejs:ostrum_leggings", [
    "CCC",
    "C C",
    "C C"
  ], {
    C: "#c:ostrum_ingots"
  });

  event.shaped("kubejs:ostrum_chestplate", [
    "C C",
    "CCC",
    "CCC"
  ], {
    C: "#c:ostrum_ingots"
  });
  

  event.smithing("kubejs:ostrum_pickaxe", "kubejs:desh_pickaxe", "#c:ostrum_ingots");
  event.smithing("kubejs:ostrum_axe", "kubejs:desh_axe", "#c:ostrum_ingots");
  event.smithing("kubejs:ostrum_shovel", "kubejs:desh_shovel", "#c:ostrum_ingots");
  event.smithing("kubejs:ostrum_hoe", "kubejs:desh_hoe", "#c:ostrum_ingots");
  event.smithing("kubejs:ostrum_sword", "kubejs:desh_sword", "#c:ostrum_ingots");

  event.smithing("kubejs:ostrum_boots", "kubejs:desh_boots", "#c:ostrum_ingots");
  event.smithing("kubejs:ostrum_leggings", "kubejs:desh_leggings", "#c:ostrum_ingots");
  event.smithing("kubejs:ostrum_chestplate", "kubejs:desh_chestplate", "#c:ostrum_ingots");
  event.smithing("kubejs:ostrum_helmet", "kubejs:desh_helmet", "#c:ostrum_ingots");
  
});
