ServerEvents.recipes(event => {
  
  event.shaped("kubejs:bronze_pickaxe", [
    "CCC",
    " S ",
    " S "
  ], {
    C: "#c:bronze_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:bronze_axe", [
    "CC ",
    "CS ",
    " S "
  ], {
    C: "#c:bronze_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:bronze_hoe", [
    "CC ",
    " S ",
    " S "
  ], {
    C: "#c:bronze_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:bronze_sword", [
    " C ",
    " C ",
    " S "
  ], {
    C: "#c:bronze_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:bronze_shovel", [
    " C ",
    " S ",
    " S "
  ], {
    C: "#c:bronze_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:bronze_helmet", [
    "CCC",
    "C C",
    "   "
  ], {
    C: "#c:bronze_ingots"
  });

  event.shaped("kubejs:bronze_boots", [
    "C C",
    "C C",
    "   "
  ], {
    C: "#c:bronze_ingots"
  });

  event.shaped("kubejs:bronze_leggings", [
    "CCC",
    "C C",
    "C C"
  ], {
    C: "#c:bronze_ingots"
  });

  event.shaped("kubejs:bronze_chestplate", [
    "C C",
    "CCC",
    "CCC"
  ], {
    C: "#c:bronze_ingots"
  });

  //De cobre a bronce
  event.smithing("kubejs:bronze_pickaxe", "kubejs:copper_pickaxe", "#c:bronze_ingots");
  event.smithing("kubejs:bronze_axe", "kubejs:copper_axe", "#c:bronze_ingots");
  event.smithing("kubejs:bronze_shovel", "kubejs:copper_shovel", "#c:bronze_ingots");
  event.smithing("kubejs:bronze_hoe", "kubejs:copper_hoe", "#c:bronze_ingots");
  event.smithing("kubejs:bronze_sword", "kubejs:copper_sword", "#c:bronze_ingots");

  event.smithing("kubejs:bronze_boots", "kubejs:copper_boots", "#c:bronze_ingots");
  event.smithing("kubejs:bronze_leggings", "kubejs:copper_leggings", "#c:bronze_ingots");
  event.smithing("kubejs:bronze_chestplate", "kubejs:copper_chestplate", "#c:bronze_ingots");
  event.smithing("kubejs:bronze_helmet", "kubejs:copper_helmet", "#c:bronze_ingots");

  //De bronce a hierro
  event.smithing("minecraft:iron_pickaxe", "kubejs:bronze_pickaxe", "#c:iron_ingots");
  event.smithing("minecraft:iron_axe", "kubejs:bronze_axe", "#c:iron_ingots");
  event.smithing("minecraft:iron_shovel", "kubejs:bronze_shovel", "#c:iron_ingots");
  event.smithing("minecraft:iron_hoe", "kubejs:bronze_hoe", "#c:iron_ingots");
  event.smithing("minecraft:iron_sword", "kubejs:bronze_sword", "#c:iron_ingots");

  event.smithing("minecraft:iron_boots", "kubejs:bronze_boots", "#c:iron_ingots");
  event.smithing("minecraft:iron_leggings", "kubejs:bronze_leggings", "#c:iron_ingots");
  event.smithing("minecraft:iron_chestplate", "kubejs:bronze_chestplate", "#c:iron_ingots");
  event.smithing("minecraft:iron_helmet", "kubejs:bronze_helmet", "#c:iron_ingots");
  
});
