ServerEvents.recipes(event => {
  
  event.shaped("kubejs:steel_pickaxe", [
    "CCC",
    " S ",
    " S "
  ], {
    C: "#c:steel_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:steel_axe", [
    "CC ",
    "CS ",
    " S "
  ], {
    C: "#c:steel_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:steel_hoe", [
    "CC ",
    " S ",
    " S "
  ], {
    C: "#c:steel_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:steel_sword", [
    " C ",
    " C ",
    " S "
  ], {
    C: "#c:steel_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:steel_shovel", [
    " C ",
    " S ",
    " S "
  ], {
    C: "#c:steel_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:steel_helmet", [
    "CCC",
    "C C",
    "   "
  ], {
    C: "#c:steel_ingots"
  });

  event.shaped("kubejs:steel_boots", [
    "C C",
    "C C",
    "   "
  ], {
    C: "#c:steel_ingots"
  });

  event.shaped("kubejs:steel_leggings", [
    "CCC",
    "C C",
    "C C"
  ], {
    C: "#c:steel_ingots"
  });

  event.shaped("kubejs:steel_chestplate", [
    "C C",
    "CCC",
    "CCC"
  ], {
    C: "#c:steel_ingots"
  });

  //De hierro a acero
  event.smithing("kubejs:steel_pickaxe", "minecraft:iron_pickaxe", "#c:steel_ingots");
  event.smithing("kubejs:steel_axe", "minecraft:iron_axe", "#c:steel_ingots");
  event.smithing("kubejs:steel_shovel", "minecraft:iron_shovel", "#c:steel_ingots");
  event.smithing("kubejs:steel_hoe", "minecraft:iron_hoe", "#c:steel_ingots");
  event.smithing("kubejs:steel_sword", "minecraft:iron_sword", "#c:steel_ingots");

  event.smithing("kubejs:steel_boots", "minecraft:iron_boots", "#c:steel_ingots");
  event.smithing("kubejs:steel_leggings", "minecraft:iron_leggings", "#c:steel_ingots");
  event.smithing("kubejs:steel_chestplate", "minecraft:iron_chestplate", "#c:steel_ingots");
  event.smithing("kubejs:steel_helmet", "minecraft:iron_helmet", "#c:steel_ingots");


  //De acero a diamante
  event.smithing("minecraft:diamond_pickaxe", "kubejs:steel_pickaxe", "#c:diamonds");
  event.smithing("minecraft:diamond_axe", "kubejs:steel_axe", "#c:diamonds");
  event.smithing("minecraft:diamond_shovel", "kubejs:steel_shovel", "#c:diamonds");
  event.smithing("minecraft:diamond_hoe", "kubejs:steel_hoe", "#c:diamonds");
  event.smithing("minecraft:diamond_sword", "kubejs:steel_sword", "#c:diamonds");

  event.smithing("minecraft:diamond_boots", "kubejs:steel_boots", "#c:diamonds");
  event.smithing("minecraft:diamond_leggings", "kubejs:steel_leggings", "#c:diamonds");
  event.smithing("minecraft:diamond_chestplate", "kubejs:steel_chestplate", "#c:diamonds");
  event.smithing("minecraft:diamond_helmet", "kubejs:steel_helmet", "#c:diamonds");
  
});
