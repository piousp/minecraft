ServerEvents.recipes(event => {
  
  event.shaped("kubejs:desh_pickaxe", [
    "CCC",
    " S ",
    " S "
  ], {
    C: "#c:desh_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:desh_axe", [
    "CC ",
    "CS ",
    " S "
  ], {
    C: "#c:desh_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:desh_hoe", [
    "CC ",
    " S ",
    " S "
  ], {
    C: "#c:desh_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:desh_sword", [
    " C ",
    " C ",
    " S "
  ], {
    C: "#c:desh_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:desh_shovel", [
    " C ",
    " S ",
    " S "
  ], {
    C: "#c:desh_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:desh_helmet", [
    "CCC",
    "C C",
    "   "
  ], {
    C: "#c:desh_ingots"
  });

  event.shaped("kubejs:desh_boots", [
    "C C",
    "C C",
    "   "
  ], {
    C: "#c:desh_ingots"
  });

  event.shaped("kubejs:desh_leggings", [
    "CCC",
    "C C",
    "C C"
  ], {
    C: "#c:desh_ingots"
  });

  event.shaped("kubejs:desh_chestplate", [
    "C C",
    "CCC",
    "CCC"
  ], {
    C: "#c:desh_ingots"
  });
  

  event.smithing("kubejs:desh_pickaxe", "minecraft:golden_pickaxe", "#c:desh_ingots");
  event.smithing("kubejs:desh_axe", "minecraft:golden_axe", "#c:desh_ingots");
  event.smithing("kubejs:desh_shovel", "minecraft:golden_shovel", "#c:desh_ingots");
  event.smithing("kubejs:desh_hoe", "minecraft:golden_hoe", "#c:desh_ingots");
  event.smithing("kubejs:desh_sword", "minecraft:golden_sword", "#c:desh_ingots");

  event.smithing("kubejs:desh_boots", "minecraft:golden_boots", "#c:desh_ingots");
  event.smithing("kubejs:desh_leggings", "minecraft:golden_leggings", "#c:desh_ingots");
  event.smithing("kubejs:desh_chestplate", "minecraft:golden_chestplate", "#c:desh_ingots");
  event.smithing("kubejs:desh_helmet", "minecraft:golden_helmet", "#c:desh_ingots");
  
});
