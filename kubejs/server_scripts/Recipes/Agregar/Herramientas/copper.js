ServerEvents.recipes(event => {
  
  event.shaped("kubejs:copper_pickaxe", [
    "CCC",
    " S ",
    " S "
  ], {
    C: "#c:copper_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:copper_axe", [
    "CC ",
    "CS ",
    " S "
  ], {
    C: "#c:copper_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:copper_hoe", [
    "CC ",
    " S ",
    " S "
  ], {
    C: "#c:copper_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:copper_sword", [
    " C ",
    " C ",
    " S "
  ], {
    C: "#c:copper_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:copper_shovel", [
    " C ",
    " S ",
    " S "
  ], {
    C: "#c:copper_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:copper_helmet", [
    "CCC",
    "C C",
    "   "
  ], {
    C: "#c:copper_ingots"
  });

  event.shaped("kubejs:copper_boots", [
    "C C",
    "C C",
    "   "
  ], {
    C: "#c:copper_ingots"
  });

  event.shaped("kubejs:copper_leggings", [
    "CCC",
    "C C",
    "C C"
  ], {
    C: "#c:copper_ingots"
  });

  event.shaped("kubejs:copper_chestplate", [
    "C C",
    "CCC",
    "CCC"
  ], {
    C: "#c:copper_ingots"
  });
  

  event.smithing("kubejs:copper_pickaxe", "minecraft:stone_pickaxe", "#c:copper_ingots");
  event.smithing("kubejs:copper_axe", "minecraft:stone_axe", "#c:copper_ingots");
  event.smithing("kubejs:copper_shovel", "minecraft:stone_shovel", "#c:copper_ingots");
  event.smithing("kubejs:copper_hoe", "minecraft:stone_hoe", "#c:copper_ingots");
  event.smithing("kubejs:copper_sword", "minecraft:stone_sword", "#c:copper_ingots");

  event.smithing("kubejs:copper_boots", "minecraft:leather_boots", "#c:copper_ingots");
  event.smithing("kubejs:copper_leggings", "minecraft:leather_leggings", "#c:copper_ingots");
  event.smithing("kubejs:copper_chestplate", "minecraft:leather_chestplate", "#c:copper_ingots");
  event.smithing("kubejs:copper_helmet", "minecraft:leather_helmet", "#c:copper_ingots");
  
});
