ServerEvents.recipes(event => {
  
  event.shaped("kubejs:titanium_pickaxe", [
    "CCC",
    " S ",
    " S "
  ], {
    C: "#c:titanium_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:titanium_axe", [
    "CC ",
    "CS ",
    " S "
  ], {
    C: "#c:titanium_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:titanium_hoe", [
    "CC ",
    " S ",
    " S "
  ], {
    C: "#c:titanium_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:titanium_sword", [
    " C ",
    " C ",
    " S "
  ], {
    C: "#c:titanium_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:titanium_shovel", [
    " C ",
    " S ",
    " S "
  ], {
    C: "#c:titanium_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:titanium_helmet", [
    "CCC",
    "C C",
    "   "
  ], {
    C: "#c:titanium_ingots"
  });

  event.shaped("kubejs:titanium_boots", [
    "C C",
    "C C",
    "   "
  ], {
    C: "#c:titanium_ingots"
  });

  event.shaped("kubejs:titanium_leggings", [
    "CCC",
    "C C",
    "C C"
  ], {
    C: "#c:titanium_ingots"
  });

  event.shaped("kubejs:titanium_chestplate", [
    "C C",
    "CCC",
    "CCC"
  ], {
    C: "#c:titanium_ingots"
  });

  //De diamante a titanio
  event.smithing("kubejs:titanium_pickaxe", "minecraft:diamond_pickaxe", "#c:titanium_ingots");
  event.smithing("kubejs:titanium_axe", "minecraft:diamond_axe", "#c:titanium_ingots");
  event.smithing("kubejs:titanium_shovel", "minecraft:diamond_shovel", "#c:titanium_ingots");
  event.smithing("kubejs:titanium_hoe", "minecraft:diamond_hoe", "#c:titanium_ingots");
  event.smithing("kubejs:titanium_sword", "minecraft:diamond_sword", "#c:titanium_ingots");

  event.smithing("kubejs:titanium_boots", "minecraft:diamond_boots", "#c:titanium_ingots");
  event.smithing("kubejs:titanium_leggings", "minecraft:diamond_leggings", "#c:titanium_ingots");
  event.smithing("kubejs:titanium_chestplate", "minecraft:diamond_chestplate", "#c:titanium_ingots");
  event.smithing("kubejs:titanium_helmet", "minecraft:diamond_helmet", "#c:titanium_ingots");

  //De titanio a netherite
  event.smithing("minecraft:netherite_pickaxe", "kubejs:titanium_pickaxe", "#c:netherite_ingots");
  event.smithing("minecraft:netherite_axe", "kubejs:titanium_axe", "#c:netherite_ingots");
  event.smithing("minecraft:netherite_shovel", "kubejs:titanium_shovel", "#c:netherite_ingots");
  event.smithing("minecraft:netherite_hoe", "kubejs:titanium_hoe", "#c:netherite_ingots");
  event.smithing("minecraft:netherite_sword", "kubejs:titanium_sword", "#c:netherite_ingots");

  event.smithing("minecraft:netherite_boots", "kubejs:titanium_boots", "#c:netherite_ingots");
  event.smithing("minecraft:netherite_leggings", "kubejs:titanium_leggings", "#c:netherite_ingots");
  event.smithing("minecraft:netherite_chestplate", "kubejs:titanium_chestplate", "#c:netherite_ingots");
  event.smithing("minecraft:netherite_helmet", "kubejs:titanium_helmet", "#c:netherite_ingots");
  
});
