ServerEvents.recipes(event => {
  
  event.shaped("kubejs:calorite_pickaxe", [
    "CCC",
    " S ",
    " S "
  ], {
    C: "#c:calorite_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:calorite_axe", [
    "CC ",
    "CS ",
    " S "
  ], {
    C: "#c:calorite_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:calorite_hoe", [
    "CC ",
    " S ",
    " S "
  ], {
    C: "#c:calorite_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:calorite_sword", [
    " C ",
    " C ",
    " S "
  ], {
    C: "#c:calorite_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:calorite_shovel", [
    " C ",
    " S ",
    " S "
  ], {
    C: "#c:calorite_ingots",
    S: "minecraft:stick"
  });

  event.shaped("kubejs:calorite_helmet", [
    "CCC",
    "C C",
    "   "
  ], {
    C: "#c:calorite_ingots"
  });

  event.shaped("kubejs:calorite_boots", [
    "C C",
    "C C",
    "   "
  ], {
    C: "#c:calorite_ingots"
  });

  event.shaped("kubejs:calorite_leggings", [
    "CCC",
    "C C",
    "C C"
  ], {
    C: "#c:calorite_ingots"
  });

  event.shaped("kubejs:calorite_chestplate", [
    "C C",
    "CCC",
    "CCC"
  ], {
    C: "#c:calorite_ingots"
  });
  

  event.smithing("kubejs:calorite_pickaxe", "kubejs:ostrum_pickaxe", "#c:calorite_ingots");
  event.smithing("kubejs:calorite_axe", "kubejs:ostrum_axe", "#c:calorite_ingots");
  event.smithing("kubejs:calorite_shovel", "kubejs:ostrum_shovel", "#c:calorite_ingots");
  event.smithing("kubejs:calorite_hoe", "kubejs:ostrum_hoe", "#c:calorite_ingots");
  event.smithing("kubejs:calorite_sword", "kubejs:ostrum_sword", "#c:calorite_ingots");

  event.smithing("kubejs:calorite_boots", "kubejs:ostrum_boots", "#c:calorite_ingots");
  event.smithing("kubejs:calorite_leggings", "kubejs:ostrum_leggings", "#c:calorite_ingots");
  event.smithing("kubejs:calorite_chestplate", "kubejs:ostrum_chestplate", "#c:calorite_ingots");
  event.smithing("kubejs:calorite_helmet", "kubejs:ostrum_helmet", "#c:calorite_ingots");
  
});
