ServerEvents.recipes(event => {
    // con iron nuggets
    event.custom({
        "type": "modern_industrialization:implosion_compressor",
        "eu": 1,
        "duration": 10,
        "item_inputs": [{
            "item": "create:powdered_obsidian",
            "amount": 1
        }, {
            "item": "modern_industrialization:industrial_tnt",
            "amount": 1
        }],
        "item_outputs": {
            "item": "create:sturdy_sheet"
        }
    });
})


