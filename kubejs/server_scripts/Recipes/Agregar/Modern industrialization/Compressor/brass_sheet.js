ServerEvents.recipes(event => {
    event.custom({
        type: "modern_industrialization:compressor",
        item_inputs: [
            Ingredient.of("create:brass_ingot").toJson()
        ],
        item_outputs: [
            Item.of("create:brass_sheet").toJson()
        ],
        duration: 100,
        eu: 2
    });
})