ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:packer",
        "duration": 100,
        "eu": 2,
        "item_inputs": [
            {
            "amount": 9,
            "tag": "c:zinc_tiny_dusts"
            }
        ],
        "item_outputs": [
            {
            "amount": 1,
            "item": "kubejs:zinc_dust"
            }
        ]
    });
})