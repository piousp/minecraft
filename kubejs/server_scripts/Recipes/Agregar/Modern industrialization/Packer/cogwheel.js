ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:packer",
        "duration": 100,
        "eu": 2,
        "item_inputs": [{
            item: "create:shaft"
        }, {
            "tag": "minecraft:planks"
        }],
        "item_outputs": [{
            "item": "create:cogwheel"
        }]
    });
})