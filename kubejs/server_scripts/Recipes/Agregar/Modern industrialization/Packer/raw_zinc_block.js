ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:packer",
        "duration": 100,
        "eu": 2,
        "item_inputs": [
            {
            "amount": 9,
            "tag": "c:raw_zinc_ores"
            }
        ],
        "item_outputs": [
            {
            "amount": 1,
            "item": "create:raw_zinc_block"
            }
        ]
    });
})