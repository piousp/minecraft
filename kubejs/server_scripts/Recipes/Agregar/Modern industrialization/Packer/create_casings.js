ServerEvents.recipes(event => {
    // brass casing con stripped log
    event.custom({
        "type": "modern_industrialization:packer",
        "duration": 100,
        "eu": 2,
        "item_inputs": [{
            tag: "c:brass_ingots"
        }, {
            "tag": "c:stripped_logs"
        }],
        "item_outputs": [{
            "item": "create:brass_casing"
        }]
    });
    
    // andesite casing con stripped log
    event.custom({
        "type": "modern_industrialization:packer",
        "duration": 100,
        "eu": 2,
        "item_inputs": [{
            item: "create:andesite_alloy"
        }, {
            "tag": "c:stripped_logs"
        }],
        "item_outputs": [{
            "item": "create:andesite_casing"
        }]
    });
    
    // copper casing con stripped log
    event.custom({
        "type": "modern_industrialization:packer",
        "duration": 100,
        "eu": 2,
        "item_inputs": [{
            tag: "c:copper_ingots"
        }, {
            "tag": "c:stripped_logs"
        }],
        "item_outputs": [{
            "item": "create:copper_casing"
        }]
    });
    
    // brass casing con stripped wood
    event.custom({
        "type": "modern_industrialization:packer",
        "duration": 100,
        "eu": 2,
        "item_inputs": [{
            tag: "c:brass_ingots"
        }, {
            "tag": "c:stripped_wood"
        }],
        "item_outputs": [{
            "item": "create:brass_casing"
        }]
    });
    
    // andesite casing con stripped wood
    event.custom({
        "type": "modern_industrialization:packer",
        "duration": 100,
        "eu": 2,
        "item_inputs": [{
            item: "create:andesite_alloy"
        }, {
            "tag": "c:stripped_wood"
        }],
        "item_outputs": [{
            "item": "create:andesite_casing"
        }]
    });
    
    // copper casing con stripped wood
    event.custom({
        "type": "modern_industrialization:packer",
        "duration": 100,
        "eu": 2,
        "item_inputs": [{
            tag: "c:copper_ingots"
        }, {
            "tag": "c:stripped_wood"
        }],
        "item_outputs": [{
            "item": "create:copper_casing"
        }]
    });
    
    // train casing
    event.custom({
        "type": "modern_industrialization:packer",
        "duration": 100,
        "eu": 2,
        "item_inputs": [{
            item: "create:sturdy_sheet"
        }, {
            "item": "create:brass_casing"
        }],
        "item_outputs": [{
            "item": "create:railway_casing"
        }]
    });
})