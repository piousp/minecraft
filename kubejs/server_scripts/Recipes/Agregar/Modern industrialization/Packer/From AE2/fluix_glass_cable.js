ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:packer",
        "duration": 100,
        "eu": 2,
        "item_inputs": [
            {
            "amount": 1,
            "item": "ae2:quartz_fiber"
            }, {
            "amount": 2,
            "tag": "ae2:all_fluix"
            }
        ],
        "item_outputs": [
            {
            "amount": 4,
            "item": "ae2:fluix_glass_cable"
            }
        ]
    });
})