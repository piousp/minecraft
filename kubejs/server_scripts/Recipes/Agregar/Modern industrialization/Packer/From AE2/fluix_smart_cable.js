ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:packer",
        "duration": 100,
        "eu": 2,
        "item_inputs": [
            {
            "amount": 1,
            "tag": "ae2:covered_cable"
            }, {
            "amount": 1,
            "tag": "c:redstone_dusts"
            }, {
            "amount": 1,
            "tag": "c:glowstone_dusts"
            }
        ],
        "item_outputs": [
            {
            "amount": 1,
            "item": "ae2:fluix_smart_cable"
            }
        ]
    });
})