ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:packer",
        "duration": 100,
        "eu": 2,
        "item_inputs": [
            {
            "amount": 4,
            "tag": "ae2:covered_cable"
            }
        ],
        "item_outputs": [
            {
            "amount": 1,
            "item": "ae2:fluix_covered_dense_cable"
            }
        ]
    });
})