ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:packer",
        "duration": 100,
        "eu": 2,
        "item_inputs": [
            {
            "amount": 1,
            "tag": "ae2:glass_cable"
            }, {
            "amount": 1,
            "tag": "minecraft:wool"
            }
        ],
        "item_outputs": [
            {
            "amount": 1,
            "item": "ae2:fluix_covered_cable"
            }
        ]
    });
})