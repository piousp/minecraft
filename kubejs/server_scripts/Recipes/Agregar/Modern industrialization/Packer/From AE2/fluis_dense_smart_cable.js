ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:packer",
        "duration": 100,
        "eu": 2,
        "item_inputs": [
            {
            "amount": 1,
            "tag": "ae2:covered_dense_cable"
            }, {
            "amount": 4,
            "tag": "c:redstone_dusts"
            }, {
            "amount": 4,
            "tag": "c:glowstone_dusts"
            }
        ],
        "item_outputs": [
            {
            "amount": 1,
            "item": "ae2:fluix_smart_dense_cable"
            }
        ]
    });
    
    event.custom({
        "type": "modern_industrialization:packer",
        "duration": 100,
        "eu": 2,
        "item_inputs": [
            {
            "amount": 4,
            "tag": "ae2:smart_cable"
            }
        ],
        "item_outputs": [
            {
            "amount": 1,
            "item": "ae2:fluix_smart_dense_cable"
            }
        ]
    });
})