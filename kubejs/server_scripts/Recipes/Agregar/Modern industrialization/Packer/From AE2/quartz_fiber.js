ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:packer",
        "duration": 100,
        "eu": 2,
        "item_inputs": [
            {
            "amount": 3,
            "tag": "c:certus_quartz_dusts"
            }, {
            "amount": 6,
            "tag": "c:glass"
            }
        ],
        "item_outputs": [
            {
            "amount": 3,
            "item": "ae2:quartz_fiber"
            }
        ]
    });
})