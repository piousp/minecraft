ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:vacuum_freezer",
        "eu": 16,
        "duration": 20,
        "fluid_inputs": {
            "fluid": "create:chocolate",
            "amount": 1000
        },
        "item_outputs": {
            "item": "create:bar_of_chocolate",
            amount: 3
        }
    });
})