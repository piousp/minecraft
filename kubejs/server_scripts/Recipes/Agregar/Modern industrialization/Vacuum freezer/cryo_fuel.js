ServerEvents.recipes(event => {
  event.custom({
      type: "modern_industrialization:vacuum_freezer",
      duration: 600,
      eu: 48,
      "item_inputs": {
          "item": "ad_astra:ice_shard",
          "amount": 4
        },
        "fluid_inputs": [
          {
              "fluid": "ad_astra:fuel",
              "amount": 500
          }
        ],
        "fluid_outputs": {
          "fluid": "ad_astra:cryo_fuel",
          "amount": 1000
        }
  });
});
