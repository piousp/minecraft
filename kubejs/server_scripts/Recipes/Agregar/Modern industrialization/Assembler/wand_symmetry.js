ServerEvents.recipes(event => {
    event.custom({
        type: "modern_industrialization:assembler",
        duration: 200,
        eu: 6,
        item_inputs: [{
            tag: "c:glass_blocks",
            amount: 3
        }, {
            tag: "c:ender_pearls"
        }, {
            item: "create:precision_mechanism"
        }, {
            tag: "c:brass_ingots"
        }, {
            tag: "c:obsidian"
        }],
        item_outputs: [{
            item: "create:wand_of_symmetry"
        }]
    });
})