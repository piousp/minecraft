ServerEvents.recipes(event => {
    event.custom({
        type: "modern_industrialization:assembler",
        duration: 200,
        eu: 6,
        item_inputs: [{
            item: "create:brass_hand"
        }, {
            item: "create:precision_mechanism"
        }, {
            tag: "c:brass_ingots"
        }, {
            tag: "c:wooden_rods",
            amount: 6
        }],
        item_outputs: [{
            item: "create:extendo_grip"
        }]
    });
})