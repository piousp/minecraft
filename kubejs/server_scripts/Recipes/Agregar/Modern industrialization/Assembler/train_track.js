ServerEvents.recipes(event => {
    // con iron nuggets
    event.custom({
        type: "modern_industrialization:assembler",
        item_inputs: [{
            tag: "create:sleepers"
        }, {
            tag: "c:iron_nuggets",
            amount: 2
        }],
        item_outputs: [{
            item: "create:track",
        }],
        duration: 400,
        eu: 8
    });
    
    // con zinc nuggets
    event.custom({
        type: "modern_industrialization:assembler",
        item_inputs: [{
            tag: "create:sleepers"
        }, {
            tag: "c:zinc_nuggets",
            amount: 2
        }],
        item_outputs: [{
            item: "create:track",
        }],
        duration: 400,
        eu: 8
    });
})