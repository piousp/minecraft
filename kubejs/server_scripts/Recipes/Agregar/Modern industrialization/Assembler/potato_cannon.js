ServerEvents.recipes(event => {
    event.custom({
        type: "modern_industrialization:assembler",
        duration: 200,
        eu: 6,
        item_inputs: [{
            item: "create:andesite_alloy"
        }, {
            item: "create:precision_mechanism"
        }, {
            item: "create:fluid_pipe",
            amount: 3
        }, {
            tag: "c:copper_ingots",
            amount: 2
        }],
        item_outputs: [{
            item: "create:potato_cannon"
        }]
    });
})