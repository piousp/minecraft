ServerEvents.recipes(event => {
    event.custom({
        type: "modern_industrialization:assembler",
        item_inputs: [{
            tag: "c:gold_plates"
        }, {
            item: "create:cogwheel"
        }, {
            item: "create:large_cogwheel"
        }, {
            tag: "c:iron_nuggets"
        }],
        item_outputs: [{
            item: "create:precision_mechanism",
            probability: 0.8
        }],
        duration: 400,
        eu: 6
    });
})