ServerEvents.recipes(event => {
    event.custom({
        type: "modern_industrialization:mixer",
        duration: 200,
        eu: 2,
        "item_inputs": [
            {
                tag: "c:copper_tiny_dusts"
            }, {
                tag: "c:zinc_tiny_dusts"
            }
        ],
        "item_outputs": {
            item: "kubejs:brass_tiny_dust",
            amount: 2
        }
    });
})