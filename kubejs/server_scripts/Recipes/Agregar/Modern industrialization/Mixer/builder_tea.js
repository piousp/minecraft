ServerEvents.recipes(event => {
    event.custom({
        type: "modern_industrialization:mixer",
        duration: 200,
        eu: 4,
        item_inputs: {
            "tag": "minecraft:leaves"
        },
        "fluid_inputs": [
            {
                "fluid": "minecraft:water",
                amount: 333.33
            }, {
                "fluid": "milk:still_milk",
                amount: 333.33
            }
        ],
        "fluid_outputs": {
            "fluid": "create:tea",
            amount: 666.66
        }
    });
})