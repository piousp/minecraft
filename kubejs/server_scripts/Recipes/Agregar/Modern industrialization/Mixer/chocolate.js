ServerEvents.recipes(event => {
    // crear chocolate a partir de los ingredientes
    event.custom({
        type: "modern_industrialization:mixer",
        duration: 200,
        eu: 4,
        item_inputs: [
            Item.of("minecraft:cocoa_beans").toJson(),
            Item.of("minecraft:sugar").toJson()
        ],
        "fluid_inputs": {
            "fluid": "milk:still_milk",
            amount: 333.33
        },
        "fluid_outputs": {
            "fluid": "create:chocolate",
            amount: 333.33
        }
    });
    
    // derretir la barra de chocolate
    event.custom({
        type: "modern_industrialization:mixer",
        duration: 200,
        eu: 4,
        item_inputs: [
            Item.of("create:bar_of_chocolate").toJson()
        ],
        "fluid_inputs": {
            "fluid": "minecraft:lava",
            amount: 1,
            probability: 0
        },
        "fluid_outputs": {
            "fluid": "create:chocolate",
            amount: 333.33
        }
    });
})