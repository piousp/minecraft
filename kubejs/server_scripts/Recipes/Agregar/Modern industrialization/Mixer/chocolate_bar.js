ServerEvents.recipes(event => {
    event.custom({
        type: "modern_industrialization:mixer",
        duration: 200,
        eu: 4,
        "fluid_inputs": {
            "fluid": "create:chocolate",
            "amount": 333.33
        },
        item_outputs: [
            Item.of("create:bar_of_chocolate").toJson()
        ]
    });
})