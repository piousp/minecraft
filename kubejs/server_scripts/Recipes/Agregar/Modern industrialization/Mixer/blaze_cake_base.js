ServerEvents.recipes(event => {
    event.custom({
        type: "modern_industrialization:mixer",
        duration: 200,
        eu: 2,
        "item_inputs": [
            Item.of("minecraft:egg").toJson(),
            Item.of("minecraft:sugar").toJson(),
            Item.of("create:cinder_flour").toJson()
        ],
        item_outputs: [
            Item.of("create:blaze_cake_base").toJson()
        ]
    });
})