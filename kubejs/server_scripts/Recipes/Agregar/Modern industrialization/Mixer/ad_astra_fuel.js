ServerEvents.recipes(event => {
    // con zinc nugget
    event.custom({
        type: "modern_industrialization:mixer",
        duration: 400,
        eu: 24,
        "item_inputs": {
            "item": "minecraft:gunpowder",
            "amount": 1
          },
          "fluid_inputs": [
            {
                "fluid": "modern_industrialization:heavy_fuel",
                "amount": 500
            },
            {
                "fluid": "modern_industrialization:nitrogen",
                "amount": 100
            }
          ],
          "fluid_outputs": {
            "fluid": "ad_astra:fuel",
            "amount": 700
          }
    });
});
