ServerEvents.recipes(event => {
    // con zinc nugget
    event.custom({
        type: "modern_industrialization:mixer",
        duration: 100,
        eu: 2,
        "item_inputs": [
            Item.of("minecraft:andesite").toJson(),
            Item.of("minecraft:iron_nugget").toJson()
        ],
        item_outputs: [
            Item.of("create:andesite_alloy").toJson()
        ]
    });
    
    // con iron nugget
    event.custom({
        type: "modern_industrialization:mixer",
        duration: 100,
        eu: 2,
        "item_inputs": [
            Item.of("minecraft:andesite").toJson(),
            Item.of("create:zinc_nugget").toJson()
        ],
        item_outputs: [
            Item.of("create:andesite_alloy").toJson()
        ]
    });
})