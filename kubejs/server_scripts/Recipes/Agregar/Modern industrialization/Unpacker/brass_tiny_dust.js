ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:unpacker",
        "duration": 100,
        "eu": 2,
        "item_inputs": [
            {
            "amount": 1,
            "tag": "c:brass_dusts"
            }
        ],
        "item_outputs": [
            {
            "amount": 9,
            "item": "kubejs:brass_tiny_dust"
            }
        ]
    });
})