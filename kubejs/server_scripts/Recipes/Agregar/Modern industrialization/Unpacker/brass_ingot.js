ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:unpacker",
        "duration": 100,
        "eu": 2,
        "item_inputs": [
            {
            "amount": 1,
            "item": "create:brass_block"
            }
        ],
        "item_outputs": [
            {
            "amount": 9,
            "item": "create:brass_ingot"
            }
        ]
    });
})