ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:unpacker",
        "duration": 100,
        "eu": 2,
        "item_inputs": [
            {
            "amount": 1,
            "tag": "c:brass_ingots"
            }
        ],
        "item_outputs": [
            {
            "amount": 9,
            "item": "create:brass_nugget"
            }
        ]
    });
})