ServerEvents.recipes(event => {
  event.custom({
      type: "modern_industrialization:distillery",
      duration: 200,
      eu: 12,
      "fluid_inputs": [
        {
          "amount": 1000,
          "fluid": "ad_astra:oil"
        }
      ],
      "fluid_outputs": [
        {
          "amount": 500,
          "fluid": "modern_industrialization:sulfuric_light_fuel"
        }
      ]
  });

  event.custom({
    type: "modern_industrialization:distillery",
    duration: 200,
    eu: 12,
    "fluid_inputs": [
      {
        "amount": 1000,
        "fluid": "ad_astra:oil"
      }
    ],
    "fluid_outputs": [
      {
        "amount": 200,
        "fluid": "modern_industrialization:sulfuric_heavy_fuel"
      }
    ]
  });

  event.custom({
    type: "modern_industrialization:distillery",
    duration: 200,
    eu: 12,
    "fluid_inputs": [
      {
        "amount": 1000,
        "fluid": "ad_astra:oil"
      }
    ],
    "fluid_outputs": [
      {
        "amount": 300,
        "fluid": "modern_industrialization:sulfuric_naphtha"
      }
    ]
  });
});
