ServerEvents.recipes(event => {
    // from nugget
    event.custom({
        "type": "modern_industrialization:forge_hammer",
        "eu": 0,
        "duration": 0,
        "item_inputs": {
            "tag": "c:zinc_nuggets",
            "amount": 1
        },
        "item_outputs": {
            "item": "kubejs:zinc_tiny_dust",
            "amount": 1
        }
    });
})