ServerEvents.recipes(event => {
    // from ore
    event.custom({
        "type": "modern_industrialization:forge_hammer",
        "eu": 25,
        "duration": 0,
        "item_inputs": {
            "tag": "c:zinc_ores",
            "amount": 1
        },
        "item_outputs": {
            "item": "kubejs:zinc_dust",
            "amount": 4
        }
    });
    
    // from raw
    event.custom({
        "type": "modern_industrialization:forge_hammer",
        "eu": 15,
        "duration": 0,
        "item_inputs": {
            "tag": "c:raw_zinc_ores",
            "amount": 3
        },
        "item_outputs": {
            "item": "kubejs:zinc_dust",
            "amount": 4
        }
    });
    
    // from ingot
    event.custom({
        "type": "modern_industrialization:forge_hammer",
        "eu": 0,
        "duration": 0,
        "item_inputs": {
            "tag": "c:zinc_ingots",
            "amount": 1
        },
        "item_outputs": {
            "item": "kubejs:zinc_dust",
            "amount": 1
        }
    });
})