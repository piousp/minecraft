ServerEvents.recipes(event => {
    // con EU para que use la herramienta (el hammer)
    event.custom({
        "type": "modern_industrialization:forge_hammer",
        "eu": 10,
        "duration": 0,
        "item_inputs": {
            "tag": "c:zinc_ores"
        },
        "item_outputs": {
            "item": "kubejs:raw_zinc",
            "amount": 3
        }
    });
    
    // sin EU para hacerlo con la mano (sin herramienta)
    event.custom({
        "type": "modern_industrialization:forge_hammer",
        "eu": 0,
        "duration": 0,
        "item_inputs": {
            "tag": "c:zinc_ores"
        },
        "item_outputs": {
            "item": "kubejs:raw_zinc",
            "amount": 2
        }
    });
})