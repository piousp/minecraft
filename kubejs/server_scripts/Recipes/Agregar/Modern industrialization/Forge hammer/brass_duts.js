ServerEvents.recipes(event => {
    // from ingot
    event.custom({
        "type": "modern_industrialization:forge_hammer",
        "eu": 0,
        "duration": 100,
        "item_inputs": {
            "tag": "c:brass_ingots"
        },
        "item_outputs": [{
            "item": "kubejs:brass_dust",
            "amount": 1
        }]
    });
})