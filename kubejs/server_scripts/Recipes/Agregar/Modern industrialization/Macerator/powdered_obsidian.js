ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 16,
        "duration": 400,
        "item_inputs": {
            "tag": "c:obsidian",
            "amount": 1
        },
        "item_outputs": [{
            "item": "create:powdered_obsidian"
        }, {
            "item": "minecraft:obsidian",
            "probability": 0.75
        }]
    });
})