ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 100,
        "item_inputs": {
            "tag": "ad_astra:venus_sandstone"
        },
        "item_outputs": {
            "item": "ad_astra:venus_sand"
        }
    });
})