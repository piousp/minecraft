ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "tag": "create:stone_types/tuff"
        },
        "item_outputs": [{
            "item": "modern_industrialization:gold_tiny_dust",
            "probability": 0.16
        }, {
            "item": "modern_industrialization:copper_tiny_dust",
            "probability": 0.16
        }, {
            "item": "kubejs:zinc_tiny_dust",
            "probability": 0.16
        }, {
            "item": "modern_industrialization:iron_tiny_dust",
            "probability": 0.16
        }]
    });
    
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:tuff"
        },
        "item_outputs": [{
            "item": "modern_industrialization:gold_tiny_dust",
            "probability": 0.16
        }, {
            "item": "modern_industrialization:copper_tiny_dust",
            "probability": 0.16
        }, {
            "item": "kubejs:zinc_tiny_dust",
            "probability": 0.16
        }, {
            "item": "modern_industrialization:iron_tiny_dust",
            "probability": 0.16
        }]
    });
})