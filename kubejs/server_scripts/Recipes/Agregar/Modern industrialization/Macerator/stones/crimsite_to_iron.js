ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "tag": "create:stone_types/crimsite"
        },
        "item_outputs": [{
            "item": "modern_industrialization:iron_dust",
            "probability": 0.4
        }, {
            "item": "modern_industrialization:iron_tiny_dust",
            "probability": 0.4
        }]
    });
})