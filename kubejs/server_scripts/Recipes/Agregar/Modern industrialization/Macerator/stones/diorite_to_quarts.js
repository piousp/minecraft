ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "tag": "create:stone_types/diorite"
        },
        "item_outputs": [{
            "item": "modern_industrialization:quartz_dust",
            "probability": 0.25
        }]
    });
    
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:diorite"
        },
        "item_outputs": [{
            "item": "modern_industrialization:quartz_dust",
            "probability": 0.25
        }]
    });
})