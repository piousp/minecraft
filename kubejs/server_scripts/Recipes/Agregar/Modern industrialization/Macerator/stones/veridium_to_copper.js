ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "tag": "create:stone_types/veridium"
        },
        "item_outputs": [{
            "item": "modern_industrialization:copper_dust",
            "probability": 0.8
        }, {
            "item": "modern_industrialization:copper_tiny_dust",
            "probability": 0.8
        }]
    });
})