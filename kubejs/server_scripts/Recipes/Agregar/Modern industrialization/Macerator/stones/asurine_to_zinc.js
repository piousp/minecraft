ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "tag": "create:stone_types/asurine"
        },
        "item_outputs": [{
            "item": "kubejs:zinc_dust",
            "probability": 0.3
        }, {
            "item": "kubejs:zinc_tiny_dust",
            "probability": 0.3
        }]
    });
})