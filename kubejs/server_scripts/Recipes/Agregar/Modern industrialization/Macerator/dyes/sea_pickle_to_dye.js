ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:sea_pickle"
        },
        "item_outputs": [{
            "item": "minecraft:lime_dye",
            "amount": 2
        }, {
            "item": "minecraft:green_dye",
            "amount": 1,
            probability: 0.1
        }]
    });
})