ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:fern",
            "amount": 1
        },
        "item_outputs": [{
            "item": "minecraft:green_dye"
        }, {
            "item": "minecraft:wheat_seeds",
            "probability": 0.1
        }]
    });
})