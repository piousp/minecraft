ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:cactus"
        },
        "item_outputs": [{
            "item": "minecraft:green_dye",
            "amount": 2
        }, {
            "item": "minecraft:green_dye",
            "amount": 1,
            probability: 0.1
        }]
    });
})