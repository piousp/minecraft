ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 4,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:granite"
        },
        "item_outputs": {
            "item": "minecraft:red_sand"
        }
    });
})