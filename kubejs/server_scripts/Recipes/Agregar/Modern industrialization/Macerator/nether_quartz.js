ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 4,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:prismarine_crystals"
        },
        "item_outputs": [{
            "item": "minecraft:quartz"
        }, {
            "item": "minecraft:quartz",
            "amount": 2,
            probability: 0.5
        }, {
            "item": "minecraft:glowstone_dust",
            "amount": 2,
            probability: 0.1
        }]
    });
})