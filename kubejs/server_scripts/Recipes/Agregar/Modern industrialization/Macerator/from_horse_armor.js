ServerEvents.recipes(event => {
    // leather
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 4,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:leather_horse_armor"
        },
        "item_outputs": [{
            "item": "minecraft:leather",
            amount: 2
        }, {
            "item": "minecraft:leather",
            amount: 2,
            "probability": 0.5
        }]
    });
    
    // iron
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 8,
        "duration": 400,
        "item_inputs": {
            "item": "minecraft:iron_horse_armor"
        },
        "item_outputs": [{
            "item": "modern_industrialization:iron_dust",
            amount: 2
        }, {
            "item": "modern_industrialization:iron_dust",
            amount: 1,
            "probability": 0.5
        }, {
            "item": "minecraft:string",
            amount: 4,
            "probability": 0.25
        }, {
            "item": "modern_industrialization:iron_tiny_dust",
            amount: 4,
            "probability": 0.25
        }]
    });
    
    // gold
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 12,
        "duration": 600,
        "item_inputs": {
            "item": "minecraft:golden_horse_armor"
        },
        "item_outputs": [{
            "item": "modern_industrialization:gold_dust",
            amount: 2
        }, {
            "item": "modern_industrialization:gold_dust",
            amount: 2,
            "probability": 0.5
        }, {
            "item": "minecraft:string",
            amount: 6,
            "probability": 0.25
        }, {
            "item": "modern_industrialization:gold_tiny_dust",
            amount: 8,
            "probability": 0.25
        }]
    });
    
    // diamond
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 16,
        "duration": 800,
        "item_inputs": {
            "item": "minecraft:diamond_horse_armor"
        },
        "item_outputs": [{
            "item": "minecraft:diamond",
            amount: 1
        }, {
            "item": "minecraft:diamond",
            amount: 3,
            "probability": 0.1
        }, {
            "item": "minecraft:string",
            amount: 6,
            "probability": 0.25
        }]
    });
})