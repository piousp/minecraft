ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:tall_grass"
        },
        "item_outputs": {
            "item": "minecraft:wheat_seeds",
            "probability": 0.5
        }
    });
    
    
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:grass"
        },
        "item_outputs": {
            "item": "minecraft:wheat_seeds",
            "probability": 0.25
        }
    });
})