ServerEvents.recipes(event => {
    // from zinc_ore
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 50,
        "item_inputs": {
            "tag": "c:raw_zinc_ores"
        },
        "item_outputs": [{
            "item": "kubejs:zinc_dust",
            "amount": 1
        }, {
            "item": "kubejs:zinc_dust",
            "amount": 1,
            "probability": 0.5
        }]
    });
    
    // from zinc_ingot
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 100,
        "item_inputs": {
            "tag": "c:zinc_ingots"
        },
        "item_outputs": [{
            "item": "kubejs:zinc_dust",
            "amount": 1
        }]
    });
})