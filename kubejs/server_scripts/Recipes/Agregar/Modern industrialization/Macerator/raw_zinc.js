ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 100,
        "item_inputs": {
            "tag": "c:zinc_ores"
        },
        "item_outputs": {
            "item": "kubejs:raw_zinc",
            "amount": 3
        }
    });
})