ServerEvents.recipes(event => {
    // from block
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:amethyst_block"
        },
        "item_outputs": [{
            "item": "minecraft:amethyst_shard",
            amount: 3
        }, {
            "item": "minecraft:amethyst_shard",
            amount: 1,
            "probability": 0.60
        }]
    });
    
    // from cluster
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:amethyst_cluster"
        },
        "item_outputs": [{
            "item": "minecraft:amethyst_shard",
            amount: 7
        }, {
            "item": "minecraft:amethyst_shard",
            amount: 1,
            "probability": 0.60
        }]
    });
})