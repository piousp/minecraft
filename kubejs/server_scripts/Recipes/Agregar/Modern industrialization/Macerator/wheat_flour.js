priority: 2

ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:wheat"
        },
        "item_outputs": [{
            "item": "create:wheat_flour"
        }, {
            "item": "create:wheat_flour",
            "amount": 2,
            "probability": 0.25
        }, {
            "item": "minecraft:wheat_seeds",
            "probability": 0.25
        }]
    });
})