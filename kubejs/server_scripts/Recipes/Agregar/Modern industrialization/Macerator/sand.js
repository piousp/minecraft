ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 100,
        "item_inputs": {
            "item": "minecraft:sandstone"
        },
        "item_outputs": {
            "item": "minecraft:sand"
        }
    });
})