ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:dripstone_block",
            "amount": 1
        },
        "item_outputs": {
            "item": "minecraft:clay_ball"
        }
    });
})