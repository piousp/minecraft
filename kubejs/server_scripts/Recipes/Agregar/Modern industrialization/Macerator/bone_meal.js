ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:calcite",
            "amount": 1
        },
        "item_outputs": {
            "item": "minecraft:bone_meal",
            "probability": 0.75
        }
    });
})