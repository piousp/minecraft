ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:saddle"
        },
        "item_outputs": [{
            "item": "minecraft:leather",
            amount: 2
        }, {
            "item": "minecraft:leather",
            amount: 2,
            "probability": 0.5
        }]
    });
})