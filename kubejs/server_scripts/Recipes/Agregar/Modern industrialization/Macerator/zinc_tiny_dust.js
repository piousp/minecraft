ServerEvents.recipes(event => {
    // from nugget
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 100,
        "item_inputs": {
            "tag": "c:zinc_nuggets",
        },
        "item_outputs": [{
            "item": "kubejs:zinc_tiny_dust",
            "amount": 1
        }]
    });
})