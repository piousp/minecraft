ServerEvents.recipes(event => {
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 4,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:netherrack"
        },
        "item_outputs": [{
            "item": "create:cinder_flour"
        }, {
            "item": "create:cinder_flour",
            "amount": 1,
            probability: 0.5
        }]
    });
})