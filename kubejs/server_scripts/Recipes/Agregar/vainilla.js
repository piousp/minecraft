ServerEvents.recipes(event => {
    event.smelting("create:brass_ingot", "kubejs:brass_dust");
    event.blasting("create:brass_ingot", "kubejs:brass_dust");
    
    event.smelting("create:brass_nugget", "kubejs:brass_tiny_dust");
    event.blasting("create:brass_nugget", "kubejs:brass_tiny_dust");
    
    event.shapeless("9x kubejs:brass_tiny_dust", ["kubejs:brass_dust"]);
    event.shapeless("kubejs:brass_dust", ["9x kubejs:brass_tiny_dust"]);
    
    event.smelting("kubejs:zinc_ingot", "#c:zinc_dusts");
    event.blasting("kubejs:zinc_ingot", "#c:zinc_dusts");
    
    event.smelting("kubejs:zinc_nugget", "#c:zinc_tiny_dusts");
    event.blasting("kubejs:zinc_nugget", "#c:zinc_tiny_dusts");
    
    event.shapeless("9x kubejs:zinc_tiny_dust", ["#c:zinc_dusts"]);
    event.shapeless("kubejs:zinc_dust", ["9x #c:zinc_tiny_dusts"]);
})