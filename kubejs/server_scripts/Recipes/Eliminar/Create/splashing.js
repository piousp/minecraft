ServerEvents.recipes(event => {
    // eliminar las recetas de splashing (bulk washing)
    event.remove({type:"create:splashing", input:"minecraft:sand", output:"minecraft:clay_ball"});
    event.remove({type:"create:splashing", input:"minecraft:red_sand", output:"minecraft:gold_nugget"});
    event.remove({type:"create:splashing", input:"minecraft:gravel"});
})