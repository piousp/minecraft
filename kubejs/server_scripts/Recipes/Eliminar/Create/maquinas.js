ServerEvents.recipes(event => {
    // eliminar la máquina
    event.remove({output:"create:crushing_wheel"});
    event.remove({output:"create:mechanical_mixer"});
    event.remove({output:"create:mechanical_press"});
    event.remove({output:"create:millstone"});
    event.remove({output:"create:mechanical_crafter"});
    
    // como el mixer se eliminó este item ya no tiene uso
    event.remove({output:"create:whisk"});
    
    // eliminar las recetas de la máquina
    event.remove({type:"create:crushing"});
    event.remove({type:"create:mixing"});
    event.remove({type:"create:pressing"});
    event.remove({type:"create:compacting"});
    event.remove({type:"create:milling"});
    event.remove({type:"create:sequenced_assembly"});
    event.remove({type:"create:deploying"});
    event.remove({type:"create:item_application"});
    event.remove({type:"create:mechanical_crafting"});
})