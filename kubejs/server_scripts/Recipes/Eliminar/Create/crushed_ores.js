ServerEvents.recipes(event => {
    // eliminar las recetas
    event.remove({input:"create:crushed_zinc_ore"});
    event.remove({input:"create:crushed_lead_ore"});
    event.remove({input:"create:crushed_copper_ore"});
    event.remove({input:"create:crushed_nickel_ore"});
    event.remove({input:"create:crushed_silver_ore"});
    event.remove({input:"create:crushed_iron_ore"});
    event.remove({input:"create:crushed_gold_ore"});
    event.remove({input:"create:crushed_uranium_ore"});
    event.remove({input:"create:crushed_platinum_ore"});
    event.remove({input:"create:crushed_tin_ore"});
})