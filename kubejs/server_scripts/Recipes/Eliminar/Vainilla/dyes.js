ServerEvents.recipes(event => {
    event.remove({id: "minecraft:lime_dye_from_smelting"});
    event.remove({id: "minecraft:green_dye"});
    event.remove({id: "minecraft:pink_dye_from_pink_tulip"});
    event.remove({id: "minecraft:black_dye_from_wither_rose"});
    event.remove({id: "minecraft:pink_dye_from_peony"});
    event.remove({id: "minecraft:light_blue_dye_from_blue_orchid"});
    event.remove({id: "minecraft:blue_dye_from_cornflower"});
    event.remove({id: "minecraft:magenta_dye_from_allium"});
    event.remove({id: "minecraft:brown_dye"});
    event.remove({id: "minecraft:red_dye_from_tulip"});
    event.remove({id: "minecraft:white_dye_from_lily_of_the_valley"});
    event.remove({id: "minecraft:light_gray_dye_from_azure_bluet"});
    event.remove({id: "minecraft:red_dye_from_rose_bush"});
    event.remove({id: "minecraft:red_dye_from_beetroot"});
    event.remove({id: "minecraft:orange_dye_from_orange_tulip"});
    event.remove({id: "minecraft:yellow_dye_from_sunflower"});
    event.remove({id: "minecraft:light_gray_dye_from_white_tulip"});
    event.remove({id: "minecraft:magenta_dye_from_lilac"});
    event.remove({id: "minecraft:yellow_dye_from_dandelion"});
    event.remove({id: "minecraft:red_dye_from_poppy"});
    event.remove({id: "minecraft:white_dye"});
    event.remove({id: "minecraft:black_dye"});
    event.remove({id: "minecraft:light_gray_dye_from_oxeye_daisy"});
})