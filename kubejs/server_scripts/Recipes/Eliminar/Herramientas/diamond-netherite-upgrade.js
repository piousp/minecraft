ServerEvents.recipes(event => {

  event.remove({ id: "minecraft:netherite_sword_smithing" });
  event.remove({ id: "minecraft:netherite_axe_smithing" });
  event.remove({ id: "minecraft:netherite_shovel_smithing" });
  event.remove({ id: "minecraft:netherite_hoe_smithing" });
  event.remove({ id: "minecraft:netherite_pickaxe_smithing" });

  event.remove({ id: "minecraft:netherite_boots_smithing" });
  event.remove({ id: "minecraft:netherite_leggings_smithing" });
  event.remove({ id: "minecraft:netherite_chestplate_smithing" });
  event.remove({ id: "minecraft:netherite_helmet_smithing" });

});