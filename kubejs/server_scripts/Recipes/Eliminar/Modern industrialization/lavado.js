ServerEvents.recipes(event => {
    // eliminar las recetas de lavado (en minecraft:crafting)
    event.remove({id:"modern_industrialization:steam_age/decolor_pipes/craft/one_item"});
    event.remove({id:"modern_industrialization:steam_age/decolor_pipes/craft/one_fluid"});
    event.remove({id:"modern_industrialization:steam_age/decolor_pipes/craft/eight_item"});
    event.remove({id:"modern_industrialization:steam_age/decolor_pipes/craft/eight_fluid"});
    event.remove({id:"modern_industrialization:compat/ae2/dyes/decolor/craft/me_wire_8"});
    event.remove({id:"modern_industrialization:compat/ae2/dyes/decolor/craft/me_wire_1"});
    
    // eliminar las recetas de lavado (en modern_industrialization:mixer)
    event.remove({id:"modern_industrialization:compat/ae2/dyes/decolor/mixer/me_wire"});
    event.remove({id:"modern_industrialization:steam_age/decolor_pipes/mixer/item"});
    event.remove({id:"modern_industrialization:steam_age/decolor_pipes/mixer/fluid"});
    
    // eliminar las recetas de empapado (en modern_industrialization:mixer)
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/dirt"});
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/mud"});
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/clay"});
})