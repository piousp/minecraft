ServerEvents.recipes(event => {
    // eliminar las recetas de concreto del modern_industrialization:mixer
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/concrete/black_concrete"});
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/concrete/blue_concrete"});
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/concrete/brown_concrete"});
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/concrete/cyan_concrete"});
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/concrete/gray_concrete"});
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/concrete/green_concrete"});
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/concrete/light_blue_concrete"});
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/concrete/light_gray_concrete"});
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/concrete/lime_concrete"});
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/concrete/magenta_concrete"});
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/concrete/orange_concrete"});
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/concrete/pink_concrete"});
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/concrete/purple_concrete"});
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/concrete/red_concrete"});
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/concrete/white_concrete"});
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/concrete/yellow_concrete"});
})