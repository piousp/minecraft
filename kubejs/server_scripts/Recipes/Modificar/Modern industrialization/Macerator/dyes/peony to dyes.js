ServerEvents.recipes(event => {
    // no se pudo modificar la receta por lo que primero la voy a borrar y después agregarla a como se desea
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/dye/pink_dye"});
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:peony"
        },
        "item_outputs": [{
            "item": "minecraft:pink_dye",
            "amount": 3
        }, {
            "item": "minecraft:pink_dye",
            probability: 0.25
        }, {
            "item": "minecraft:magenta_dye",
            probability: 0.25
        }]
    });
})