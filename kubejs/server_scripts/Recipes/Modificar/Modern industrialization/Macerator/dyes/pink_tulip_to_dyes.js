ServerEvents.recipes(event => {
    // no se pudo modificar la receta por lo que primero la voy a borrar y después agregarla a como se desea
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/dye/pink_dye2"});
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:pink_tulip"
        },
        "item_outputs": [{
            "item": "minecraft:pink_dye",
            "amount": 2
        }, {
            "item": "minecraft:lime_dye",
            probability: 0.1
        }]
    });
})