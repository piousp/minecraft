ServerEvents.recipes(event => {
    // no se pudo modificar la receta por lo que primero la voy a borrar y después agregarla a como se desea
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/dye/red_dye3"});
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:rose_bush"
        },
        "item_outputs": [{
            "item": "minecraft:red_dye",
            "amount": 3
        }, {
            "item": "minecraft:red_dye",
            "amount": 2,
            probability: 0.25
        }, {
            "item": "minecraft:green_dye",
            "amount": 2,
            probability: 0.05
        }]
    });
})