ServerEvents.recipes(event => {
    // no se pudo modificar la receta por lo que primero la voy a borrar y después agregarla a como se desea
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/dye/light_blue_dye"});
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:blue_orchid"
        },
        "item_outputs": [{
            "item": "minecraft:light_blue_dye",
            "amount": 2
        }, {
            "item": "minecraft:light_gray_dye",
            probability: 0.05
        }]
    });
})