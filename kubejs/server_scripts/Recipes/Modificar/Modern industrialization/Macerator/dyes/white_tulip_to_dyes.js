ServerEvents.recipes(event => {
    // no se pudo modificar la receta por lo que primero la voy a borrar y después agregarla a como se desea
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/dye/light_gray_dye3"});
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:white_tulip"
        },
        "item_outputs": [{
            "item": "minecraft:white_dye",
            "amount": 2
        }, {
            "item": "minecraft:light_gray_dye",
            probability: 0.5
        }, {
            "item": "minecraft:lime_dye",
            probability: 0.1
        }]
    });
})