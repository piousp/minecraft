ServerEvents.recipes(event => {
    // no se pudo modificar la receta por lo que primero la voy a borrar y después agregarla a como se desea
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/dye/magenta_dye1"});
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:allium"
        },
        "item_outputs": [{
            "item": "minecraft:magenta_dye",
            "amount": 2
        }, {
            "item": "minecraft:purple_dye",
            "amount": 2,
            probability: 0.1
        }, {
            "item": "minecraft:pink_dye",
            probability: 0.1
        }]
    });
})