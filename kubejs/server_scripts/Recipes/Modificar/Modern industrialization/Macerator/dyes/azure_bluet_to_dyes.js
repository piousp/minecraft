ServerEvents.recipes(event => {
    // no se pudo modificar la receta por lo que primero la voy a borrar y después agregarla a como se desea
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/dye/light_gray_dye1"});
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:azure_bluet"
        },
        "item_outputs": [{
            "item": "minecraft:light_gray_dye",
            "amount": 2
        }, {
            "item": "minecraft:white_dye",
            "amount": 2,
            probability: 0.1
        }]
    });
})