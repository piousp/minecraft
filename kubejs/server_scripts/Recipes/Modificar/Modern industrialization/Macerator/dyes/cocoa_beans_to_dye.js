ServerEvents.recipes(event => {
    // no se pudo modificar la receta por lo que primero la voy a borrar y después agregarla a como se desea
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/dye/brown_dye"});
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:cocoa_beans"
        },
        "item_outputs": [{
            "item": "minecraft:brown_dye",
            "amount": 2
        }, {
            "item": "minecraft:brown_dye",
            probability: 0.1
        }]
    });
})