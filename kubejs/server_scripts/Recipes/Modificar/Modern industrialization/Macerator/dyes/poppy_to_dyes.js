ServerEvents.recipes(event => {
    // no se pudo modificar la receta por lo que primero la voy a borrar y después agregarla a como se desea
    event.remove({id:"modern_industrialization:vanilla_recipes/mixer/dye/red_dye2"});
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:poppy"
        },
        "item_outputs": [{
            "item": "minecraft:red_dye",
            "amount": 2
        }, {
            "item": "minecraft:green_dye",
            "amount": 1,
            probability: 0.05
        }]
    });
})