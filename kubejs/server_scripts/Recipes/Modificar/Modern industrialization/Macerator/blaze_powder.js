ServerEvents.recipes(event => {
    // no se pudo modificar la receta por lo que primero la voy a borrar y después agregarla a como se desea
    event.remove({id:"modern_industrialization:vanilla_recipes/macerator/blaze_dust"});
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:blaze_rod"
        },
        "item_outputs": [{
            "item": "minecraft:blaze_powder",
            "amount": 4
        }, {
            "item": "minecraft:blaze_powder",
            "amount": 2,
            probability: 0.25
        }]
    });
})