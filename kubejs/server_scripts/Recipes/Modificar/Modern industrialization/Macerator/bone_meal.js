ServerEvents.recipes(event => {
    // no se pudo modificar la receta por lo que primero la voy a borrar y después agregarla a como se desea
    event.remove({id:"modern_industrialization:vanilla_recipes/macerator/bone_meal_from_bone"});
    event.custom({
        "type": "modern_industrialization:macerator",
        "eu": 2,
        "duration": 200,
        "item_inputs": {
            "item": "minecraft:bone"
        },
        "item_outputs": [{
            "item": "minecraft:bone_meal",
            "amount": 6
        }, {
            "item": "minecraft:white_dye",
            probability: 0.25
        }]
    });
})