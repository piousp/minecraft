ServerEvents.tags("fluid", event => {
  event.add("c:oils", "ad_astra:oil");
  event.add("c:oils", "modern_industrialization:crude_oil");

  event.add("c:fuels", "ad_astra:fuel");
  event.add("c:fuels", "modern_industrialization:sulfuric_light_fuel");
  
});