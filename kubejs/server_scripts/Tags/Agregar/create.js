ServerEvents.tags('item', event => {
    // brass
    event.get("c:brass_dusts").add("kubejs:brass_dust")
    event.get("c:brass_tiny_dusts").add("kubejs:brass_tiny_dust")
    
    // zinc
    event.get("c:raw_materials").add("kubejs:raw_zinc")
    event.get("c:zinc_raw_materials").add("kubejs:raw_zinc")
    event.get("c:raw_zinc_ores").add("kubejs:raw_zinc")
    event.get("c:zinc_dusts").add("kubejs:zinc_dust")
    event.get("c:zinc_tiny_dusts").add("kubejs:zinc_tiny_dust")
    event.get("c:zinc_ingots").add("kubejs:zinc_ingot")
    event.get("c:ingots").add("kubejs:zinc_ingot")
    event.get("c:zinc_nuggets").add("kubejs:zinc_nugget")
    event.get("minecraft:beacon_payment_items").add("kubejs:zinc_ingot")
    
    // stones
    event.get("create:stone_types/crimsite").add("create:crimsite")
    event.get("create:stone_types/ochrum").add("create:ochrum")
    event.get("create:stone_types/veridium").add("create:veridium")
    event.get("create:stone_types/asurine").add("create:asurine")
})