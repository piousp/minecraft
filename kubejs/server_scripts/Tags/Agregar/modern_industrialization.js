ServerEvents.tags("item", event => {
    event.get("c:iron_rods").add("modern_industrialization:iron_rod");
    event.get("ad_astra_platform:iron_rods").add("modern_industrialization:iron_rod");
    
    // bypass el bug de teñir alfombras y shulker boxes en el mixer
    event.get("minecraft:carpets").add("#minecraft:wool_carpets");
    event.get("shulker_box").add("#c:shulker_boxes");
});