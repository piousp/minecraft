[Home](../README.md) > Armaduras >

El objetivo de este documento es tener una forma fácil de comparar los valores de las diferentes armaduras que hay en del mod pack. También puede revisar las [Herramientas y armas](Herramientas_armas.md)

# De alto uso

| | Copper | Bronze | Iron | Steel | Diamond | Titanium | Netherite |
| --- | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Durabilidad | 10 | 13 | 15 | 25 | 33 | 35 | 37 |
| Protección **pies** | 1 | 2 | 2 | 3 | 3 | 3 | 3 |
| Protección **piernas** | 3 | 4 | 5 | 5 | 6 | 6 | 6 |
| Protección **cuerpo** | 5 | 5 | 6 | 7 | 8 | 8 | 8 |
| Protección **cabeza** | 1 | 2 | 2 | 2 | 3 | 3 | 3 |
| Encantamiento | 10 | 11 | 9 | 8 | 10 | 8 | 15 |
| Sonido al equipar | minecraft:item.armor.equip_iron | minecraft:item.armor.equip_iron | minecraft:item.armor.equip_iron | minecraft:item.armor.equip_diamond | minecraft:item.armor.equip_diamond | minecraft:item.armor.equip_netherite | minecraft:item.armor.equip_netherite |
| Dureza | 0.0 | 0.0 | 0.0 | 1.0 | 2.0 | 2.0 | 3.0 |
| Resistencia al retroceso | 0.0 | 0.0 | 0.0 | 0.0 | 0.0 | 0.1 | 0.1 |
| Reparar con | c:copper:ingots | c:bronze_ingots | c:iron_ingots | c:steel_ingots | c:diamonds | c:titanium_ingots | c:netherite_ingots |

# De alta encantabilidad

| | Gold | Desh | Ostrum | Calorite |
| --- | :---: | :---: | :---: | :---: |
| Durabilidad | | 15 | 20 | 24 |
| Protección **pies** | | 2 | 3 | 3 |
| Protección **piernas** | | 6 | 7 | 7 |
| Protección **cuerpo** | | 5 | 6 | 6 |
| Protección **cabeza** | | 2 | 3 | 3 |
| Encantamiento | | 28 | 33 | 39 |
| Sonido al equipar | | minecraft:item.armor.equip_iron | minecraft:item.armor.equip_iron | minecraft:item.armor.equip_iron |
| Dureza | | 0.0 | 1.0 | 3.0 |
| Resistencia al retroceso | | 0.0 | 0.0 | 0.5 |
| Reparar con | c:gold_ingots | c:desh_ingots | c:ostrum_ingots | c:calorite_ingots |
