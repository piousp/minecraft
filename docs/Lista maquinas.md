[Home](../README.md) > Lista de máquinas >

Lista de máquinas para poder meter recetas

# Vainilla

## Bloques

- Blast furnace
- Brewing stand
- Campire
- Composter
- Crafting table
- Furnace
- Smithing table
- Smoker
- Stonecutter
- Woodcutting

## Herramientas

- Axe
- Honeycomb
- Shovel

# Create

## Simples

- [Mechanical saw](Maquinas/Create/Mechanical saw.md)

## Multi bloque

- Filling by spout
- Fan
  - [Bulk washing](Maquinas/Create/Bulk washing.md)
  - [Bulk smoking](Maquinas/Create/Bulk smoking.md)
  - [Bulk blasting](Maquinas/Create/Bulk blasting.md)
  - [Bulk haunting](Maquinas/Create/Bulk haunting.md)
- Item Draining

## Otros

- Sandpaper polishing

# Modern Industrialization

## Simples

- [Assembler](Maquinas/Modern Industrialization/Assembler.md)
- [Centrifuge](Maquinas/Modern Industrialization/Centrifuge.md)
- [Chemical reactor](Maquinas/Modern Industrialization/Chemical reactor.md)
- [Compressor](Maquinas/Modern Industrialization/Compressor.md) (tiene 3 tiers)
- [Cutting machine](Maquinas/Modern Industrialization/Cutting machine.md) (tiene 3 tiers)
- [Distillery](Maquinas/Modern Industrialization/Distillery.md)
- [Electrolizer](Maquinas/Modern Industrialization/Electrolizer.md)
- [Forge hammer](Maquinas/Modern Industrialization/Forge hammer.md)
- [Furnace](Maquinas/Modern Industrialization/Furnace.md) (tiene 3 tiers)
- [Macerator](Maquinas/Modern Industrialization/Macerator.md) (tiene 3 tiers)
- [Mixer](Maquinas/Modern Industrialization/Mixer.md) (tiene 3 tiers)
- [Packer](Maquinas/Modern Industrialization/Packer.md) (tiene 2 tiers)
- [Polarizer](Maquinas/Modern Industrialization/Polarizer.md)
- [Unpacker](Maquinas/Modern Industrialization/Unpacker.md) (tiene 2 tiers)
- [Wiremill](Maquinas/Modern Industrialization/Wiremill.md) (tiene 2 tiers)

## Multi bloque

Las máquinas multibloque pueden ser configuradas para los inputs y outputs de item y fluidos usando los hatches, por lo que se pueden construir de forma que no acepten líquidos pero si los producen. La cantidad (para items) o capacidad (para líquidos) de campos para los inputs y outputs los determina el tier del hatch. Para los fluidos se requiere 1 hatch por cada líquido que se quiere insertar/extraer.

Cuando la máquina recibe todos los insumos para empezar a procesar una receta, esta empezará a procesarlos sin importar si hay hatches para la salida de los productos, estos últimos se perderían.

- [Blast furnace](Maquinas/Modern Industrialization/Blast furnace.md) (tiene 3 tiers)
- [Coke oven](Maquinas/Modern Industrialization/Coke oven.md)
- [Distillation tower](Maquinas/Modern Industrialization/Distillation tower.md)
- [Fusion reactor](Maquinas/Modern Industrialization/Fusion reactor.md)
- [Heat exchanger](Maquinas/Modern Industrialization/Heat exchanger.md)
- [Implosion compressor](Maquinas/Modern Industrialization/Implosion compressor.md)
- [Nuclear reactor](Maquinas/Modern Industrialization/Nuclear reactor.md)
- [Oil drilling rig](Maquinas/Modern Industrialization/Oil drilling rig.md)
- [Pressurizer](Maquinas/Modern Industrialization/Pressurizer.md)
- [Quarry](Maquinas/Modern Industrialization/Quarry.md) (tiene 2 tiers)
- [Vacuum freezer](Maquinas/Modern Industrialization/Vacuum freezer.md)

## Tiers

### Tiers máquinas simples
Existen 3 tiers:
- Bronze
- Steel
- Electric

Las máquinas que solo tienen 2 tiers es porque no tiene la versión en bronce. Las que no tiene tier son eléctricas.

Los primeros 2 tiers utilizan steam para funcionar mientras que el último requiere electricidad. Todas las máquinas electricas funcionan con la corriente más básica pero se puden actualizar para que un tier de corriente más elevado.

Para indicar que una receta sirve a partir de algún tier específico se debe de colocar un uso de EU específico, se debe colocar que la receta usa 4 EU para que funcione a partir del tier steel y usar 8 EU o más para que solo funcione en el tier electrico.

### Quarry tiers

- Steam
- Electric

### Blast furnace tiers

- Steam
- Electric
  - Cupronickel coil
  - Kanthal coil

# Ad Astra

- Cryo freezer
- Fuel refinery
- Nasa workbench
- Oxygen loader
- Oxygen distributor

# Applied Energistics 2
 - ME Drive
 - Charger
 - Inscriber

# Spirit, Vitalize, Reaper
 - Soul Reaper
 - Soul Beam

# Lista de id de receta

Eventualmente todos estos ids van a estar dentro de las página de la correspondiente máquina

- minecraft:crafting_shaped
- minecraft:crafting_shapeless
- minecraft:stonecutting
- minecraft:smelting
- minecraft:blasting
- minecraft:smoking
- minecraft:campfire_cooking
- minecraft:smithing