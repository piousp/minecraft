[Home](../README.md) > Herramientas y armas >

El objetivo de este documento es tener una forma fácil de comparar los valores de las diferentes tiers de armas que hay en del mod pack. También puede revisar las [Armaduras](Armaduras.md)

# De alto uso

| | Copper | Bronze | Iron | Steel | Diamond | Titanium | Netherite |
| --- | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Usos | 200 | 250 | 250 | 770 | 1561 | 1670 | 2031 |
| Velocidad | 4.0 | 5.0 | 6.0 | 6.0 | 8.0 | 8.0 | 9.0 |
| Bonus de ataque y daño | 2.0 | 2.0 | 2.0 | 2.0 | 3.0 | 2.0 | 4.0 |
| Nivel | 2 | 2 | 2 | 3 | 3 | 4 | 4 |
| Encantamiento | 16 | 15 | 14 | 11 | 10 | 10 | 15 |
| Reparar con | c:copper:ingots | c:bronze_ingots | c:iron_ingots | c:steel_ingots | c:diamonds | c:titanium_ingots | c:netherite_ingots |

# De alta encantabilidad

| | Gold | Desh | Ostrum | Calorite |
| --- | :---: | :---: | :---: | :---: |
| Usos | 32 | 131 | 500 | 1000 |
| Velocidad | 12 | 15.0 | 20.0 | 25.0 |
| Bonus de ataque y daño | 0.0 | 1.0 | 2.0 | 3.0 |
| Nivel | 0 | 2 | 3 | 4 |
| Encantamiento | 22 | 28 | 33 | 39 |
| Reparar con | c:gold_ingots | c:desh_ingots | c:ostrum_ingots | c:calorite_ingots |
