[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Chemical reactor >

![Interfaz en REI](Images/Chemical reactor/recetaREI.png)

Es una máquina avanzada que en la que suceden reacciones químicas entre items y/o líquidos.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Chemical reactor |
| Id del proceso | modern_industrialization:chemical_reactor |
| Tiene interfaz | Si |
| Automatizable | Si |
| Acepta fluidos | Si, 3 campos |
| Acepta items | Si, 3 campos |
| Produce fluidos | Si, 3 campos |
| Produce items | Si, 3 campos |

# Cuando usarla

Cuando se necesita combinar químicamente items y/o líquidos

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Chemical reactor/Colocado.png) |
| Funcionando | ![Funcionando](Images/Chemical reactor/Funcionando.png) |
| Líquidos | ![Líquidos](Images/Chemical reactor/Líquidos.png) |
| Soluciones | ![Soluciones](Images/Chemical reactor/Soluciones.png) |

# Modificaciones hechas

No se han hecho modificaciones

# Ejemplo de receta

## Solución
```json
{
  "type": "modern_industrialization:chemical_reactor",
  "eu": 8,
  "duration": 200,
  "item_inputs" : [
    {"item" :  "modern_industrialization:manganese_crushed_dust",
      "amount": 1}
  ],
  "fluid_inputs" : {
    "fluid" : "modern_industrialization:sulfuric_acid",
    "amount": 9000
  },
  "fluid_outputs" : {
    "fluid" : "modern_industrialization:manganese_sulfuric_solution",
    "amount" : 9000
  }
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/main/resources/data/modern_industrialization/recipes/materials/chemical_reactor/manganese_sulfuric_solution.json)

## Item

```json
{
  "type": "modern_industrialization:chemical_reactor",
  "eu": 12,
  "duration": 400,
  "item_inputs": [
    {
      "item" : "modern_industrialization:silicon_wafer",
      "amount": 1
    },
    {
      "tag" : "c:antimony_dusts",
      "amount": 1
    },
    {
      "tag" : "c:aluminum_dusts",
      "amount": 1
    }
  ],

  "fluid_inputs": [
    {
      "fluid" : "modern_industrialization:argon",
      "amount" : 250
    },
    {
      "fluid" : "modern_industrialization:styrene_butadiene_rubber",
      "amount" : 500
    }
  ],
  "item_outputs": {
    "item" : "modern_industrialization:random_access_memory",
    "amount": 2
  }
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/main/resources/data/modern_industrialization/recipes/electric_age/component/chemical_reactor/random_acces_memory.json)