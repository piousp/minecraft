[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Wiremill >

![Interfaz en REI](Images/Wiremill/recetaREI.png)

Es una máquina especializada en hacer alambres de matel.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Wiremill |
| Id del proceso | modern_industrialization:wiremill |
| Tiene interfaz | Si |
| Automatizable | Si |
| Acepta fluidos | No |
| Acepta items | Si, 1 campo |
| Produce fluidos | No |
| Produce items | Si, 1 campo |

# Cuando usarla

- Para hacer alambres de plates
- Para hacer hilos de alambres

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Wiremill/Colocado.png) |
| Funcionando | ![Funcionando](Images/Wiremill/Funcionando.png) |
| Alambres | ![Alambres](Images/Wiremill/Alambres.png) |

# Modificaciones hechas

No se han hecho modificaciones

# Ejemplo de receta

## Alambre
```json
{
  "type": "modern_industrialization:wiremill",
  "duration": 200,
  "eu": 2,
  "item_inputs": [
    {
      "amount": 1,
      "tag": "c:aluminum_plates"
    }
  ],
  "item_outputs": [
    {
      "amount": 2,
      "item": "modern_industrialization:aluminum_wire"
    }
  ]
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/generated/resources/data/modern_industrialization/recipes/materials/aluminum/wiremill/wire.json)

## Hilo
```json
{
  "type": "modern_industrialization:wiremill",
  "duration": 100,
  "eu": 2,
  "item_inputs": [
    {
      "amount": 1,
      "item": "modern_industrialization:copper_wire"
    }
  ],
  "item_outputs": [
    {
      "amount": 4,
      "item": "modern_industrialization:copper_fine_wire"
    }
  ]
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/generated/resources/data/modern_industrialization/recipes/materials/copper/wiremill/fine_wire.json)