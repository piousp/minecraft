[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Quarry >

![Interfaz en REI](Images/Quarry/recetaREI.png)

Es una máquina multibloque utiliza *drillers* para perforar y buscar piedras, minerales, etc, dependiendo del material del driller así son los bloques que se obtienen. Tiene 2 tiers, el primero usa steam, el otro es eléctrico.

A diferencia del quarry de Buildcraft o al miner de Mekanism, los bloques resultantes del funcionamiento del quarry aparecen por arte de magia (es una simple receta).

| | Valor |
| --- | ----------- |
| Nombre del proceso | Quarry |
| Id del proceso | modern_industrialization:quarry |
| Tiene interfaz | Si |
| Automatizable | Si |
| Largo x Ancho x Altura | 3 x 3 x 5 |

# Cuando usarla

- Para obtener recursos sin tener que hacer espeleología

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Quarry/Colocado.png) |
| Funcionando | ![Funcionando](Images/Quarry/Funcionando.png) |
| Piedras | ![Obsidian](Images/Quarry/Piedras.png) |

# Modificaciones hechas

No se han hecho modificaciones

# Ejemplo de receta

```json
{
  "type": "modern_industrialization:quarry",
  "eu": 32,
  "duration": 600,
  "item_inputs": {
    "item": "modern_industrialization:stainless_steel_drill",
    "amount": 1,
    "probability": 0.04
  },
  "item_outputs": [
    {
      "item": "modern_industrialization:titanium_ore",
      "amount": 1,
      "probability": 0.15
    },
    {
      "item": "modern_industrialization:tungsten_ore",
      "amount": 1,
      "probability": 0.2
    },
    {
      "item": "modern_industrialization:mozanite_ore",
      "amount": 1,
      "probability": 0.25
    },
    {
      "item": "modern_industrialization:platinum_ore",
      "amount": 1,
      "probability": 0.12
    }
  ]
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/main/resources/data/modern_industrialization/recipes/quarry/stainless_steel.json)
