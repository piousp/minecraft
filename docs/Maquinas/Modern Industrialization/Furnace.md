[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Furnace >

![Interfaz en REI](Images/Furnace/recetaREI.png)

Esta es una de las máquinas básicas, en escencia es hace lo mismo que el horno vainilla pero puede ser mejorable y tener más velocidad.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Furnace |
| Id del proceso | modern_industrialization:furnace |
| Tiene interfaz | Si |
| Automatizable | Si |
| Acepta fluidos | No |
| Acepta items | Si, 1 campos |
| Produce fluidos | No |
| Produce items | Si, 1 campos |

# Cuando usarla

Se usa para:

- **Cocinar** comida
- **Fundir** metales
- Todas las recetas de `minecraft:smelting`

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Furnace/Colocado.png) |
| Cocinar | ![Cocinar](Images/Furnace/Cocinar.png) |
| Despolarizar | ![Despolarizar](Images/Furnace/Despolarizar.png) |
| Funcionando | ![Funcionando](Images/Furnace/Funcionando.png) |

# Modificaciones hechas

No se han hecho modificaciones, pero si se agregaron recetas a `minecraft:smelting`
