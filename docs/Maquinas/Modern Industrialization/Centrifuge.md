[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Centrifuge >

![Interfaz en REI](Images/Centrifuge/recetaREI.png)

Es una máquina avanzada que usa centrifugación para separar componentes.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Centrifuge |
| Id del proceso | modern_industrialization:centrifuge |
| Tiene interfaz | Si |
| Automatizable | Si |
| Acepta fluidos | Si, 1 campo |
| Acepta items | Si, 1 campo |
| Produce fluidos | Si, 4 campos |
| Produce items | Si, 4 campos |

# Cuando usarla

Para seperar componentes del item/fluido de entrada en varios medienta el centrifugado

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Centrifuge/Colocado.png) |
| Funcionando | ![Funcionando](Images/Centrifuge/Funcionando.png) |
| Líquidos | ![Líquidos](Images/Centrifuge/Líquidos.png) |
| Flint | ![Flint](Images/Centrifuge/Flint.png) |

# Modificaciones hechas

No se han hecho modificaciones

# Ejemplo de receta

## Gases
```json
{
  "type": "modern_industrialization:centrifuge",
  "eu" : 24,
  "duration" : 600,

  "fluid_inputs" : {
    "fluid" : "modern_industrialization:liquid_air",
    "amount" : 3000
  },

  "fluid_outputs" : [
    {
      "fluid" : "modern_industrialization:oxygen",
      "amount" : 650
    },

    {
      "fluid" : "modern_industrialization:nitrogen",
      "amount" : 2315
    },
    {
      "fluid" : "modern_industrialization:argon",
      "amount" : 35
    }
  ]

}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/main/resources/data/modern_industrialization/recipes/materials/centrifuge/liquid_air.json)

## Item

```json
{
  "type": "modern_industrialization:centrifuge",
  "eu": 8,
  "duration": 300,
  "item_inputs": {
    "item": "minecraft:gravel",
    "amount": 1
  },
  "item_outputs": {
    "item": "minecraft:flint",
    "amount": 2
  }
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/main/resources/data/modern_industrialization/recipes/materials/centrifuge/flint.json)