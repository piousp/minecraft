[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Polarizer >

![Interfaz en REI](Images/Polarizer/recetaREI.png)

Es una máquina especializada que se utiliza para imantar items.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Polarizer |
| Id del proceso | modern_industrialization:polarizer |
| Tiene interfaz | Si |
| Automatizable | Si |
| Acepta fluidos | No |
| Acepta items | Si, 2 campos |
| Produce fluidos | No |
| Produce items | Si, 1 campo |

# Cuando usarla

- Cuando ocupa imantar items

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Polarizer/Colocado.png) |
| Funcionando | ![Funcionando](Images/Polarizer/Funcionando.png) |
| Imanes | ![Imanes](Images/Polarizer/Imanes.png) |

# Modificaciones hechas

No se han hecho modificaciones

# Ejemplo de receta

```json
{
  "type": "modern_industrialization:polarizer",
  "duration": 200,
  "eu": 8,
  "item_inputs": [
    {
      "amount": 1,
      "item": "modern_industrialization:steel_rod"
    }
  ],
  "item_outputs": [
    {
      "amount": 1,
      "item": "modern_industrialization:steel_rod_magnetic"
    }
  ]
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/generated/resources/data/modern_industrialization/recipes/materials/steel/polarizer/rod_magnetic.json)
