[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Mixer >

![Interfaz en REI](Images/Mixer/recetaREI.png)

Esta es una de las máquinas básicas, funciona como una batidora que mezcla ingredientes, entre sólidos y líquidos.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Mixer |
| Id del proceso | modern_industrialization:mixer |
| Tiene interfaz | Si |
| Automatizable | Si |
| Acepta fluidos | Si, 2 campos |
| Acepta items | Si, 4 campos |
| Produce fluidos | Si, 2 campos |
| Produce items | Si, 2 campos |

# Cuando usarla

Se usa para:

- **Mezclar polvos** para hacer aleaciones
- **Mezclar ingredientes** para crear pólvora, concrete powder, otros colores de tintes
- **Teñir items** con el color del tinte deseado
- **Crear líquidos** (soluciones) como el lubricante que use el [cutting machine](Cutting machine.md)
- **Generar piedras** con 1 mb de agua, 1 mb de lava y 1 piedra se puede generar 2 piedras (teóricamente consume 1 para generar 2)

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Mixer/colocado.png) |
| Electrum | ![Electrum](Images/Mixer/Electrum.png) |

# Modificaciones hechas

## Recetas agregadas

- Creación de la aleación de brass y andesite
- Hacer tea y chocolate (de create)

## Recetas eliminadas

Estas 3 se pasaron al [bulk washing](../Create/Bulk washing.md)

- Las que eliminaban el color con agua
- El mezclar el concrete powder con agua
- Items que se empapaban como crear mud

# Ejemplo de receta

## Cambiar de color
```json
{
  "type": "modern_industrialization:mixer",
  "duration": 100,
  "eu": 2,
  "item_inputs": [
    {
      "amount": 1,
      "tag": "c:blue_dyes"
    },
    {
      "amount": 1,
      "tag": "c:shulker_box"
    }
  ],
  "item_outputs": [
    {
      "amount": 1,
      "item": "minecraft:blue_shulker_box"
    }
  ]
}
```
[Fuente (23/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/1.6.3/src/generated/resources/data/modern_industrialization/recipes/dyes/blue/mixer/shulker_box.json)

## Procesar líquidos
```json
{
  "type": "modern_industrialization:mixer",
  "eu": 2,
  "duration": 100,
  "item_inputs": {
    "item": "minecraft:redstone",
    "amount": 1
  },
  "fluid_inputs": {
    "fluid": "modern_industrialization:creosote",
    "amount": 500
  },
  "fluid_outputs": {
    "fluid": "modern_industrialization:lubricant",
    "amount": 500
  }
}
```
[Fuente (23/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/main/resources/data/modern_industrialization/recipes/materials/mixer/lubricant.json)