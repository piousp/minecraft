[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Pressurizer >

![Interfaz en REI](Images/Pressurizer/recetaREI.png)

Es una máquina multibloque para comprimir gases y liquidos en su variante de alta presión, también puede despresurizar para regresar a la variante de presion normal.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Pressurizer |
| Id del proceso | modern_industrialization:pressurizer |
| Tiene interfaz | Si |
| Automatizable | Si |
| Largo x Ancho x Altura | 3 x 3 x 5 (en cruz +) |

# Cuando usarla

- Para intercambiar entre líquidos y gases a alta o baja presión

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Pressurizer/Colocado.png) |
| Funcionando | ![Funcionando](Images/Pressurizer/Funcionando.png) |
| Aire líquido | ![Aire líquido](Images/Pressurizer/Aire líquido.png) |

# Modificaciones hechas

No se han hecho modificaciones

# Ejemplo de receta

```json
{
  "type" : "modern_industrialization:pressurizer",
  "eu" : 2,
  "duration" : 100,

  "fluid_inputs" : {
    "fluid" : "modern_industrialization:high_pressure_steam",
    "amount" : 125
  },

  "fluid_outputs" : {
    "fluid" : "modern_industrialization:steam",
    "amount" : 1000
  }

}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/main/resources/data/modern_industrialization/recipes/materials/pressurizer/down/steam.json)
