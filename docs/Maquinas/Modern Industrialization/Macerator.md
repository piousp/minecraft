[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Macerator >

![Interfaz en REI](Images/Macerator/recetaRei.png)

Esta es una de las máquinas básicas, funciona como un molino para demoler lo que entra.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Macerator |
| Id del proceso | modern_industrialization:macerator |
| Tiene interfaz | Si |
| Automatizable | Si |
| Acepta fluidos | No |
| Acepta items | Si, 1 campo |
| Produce fluidos | No |
| Produce items | Si, 4 campos |

# Cuando usarla

Reemplaza al [Forge hammer](Forge hammer.md), se usa para:

- **Procesar los ores** hasta dejarlos en los polvos.
- **Moler los items metálicos** puede moler lingotes, nuggets, rods, bolts, plates, rings y curved plates, entre otras cosas.
- **Moler las flores** para obtener tintes
- Cualquier otra receta que en esencial requiere moler como crear arena, gravel o cobblestone.

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Macerator/colocado.png) |
| Receta de ores | ![Ores](Images/Macerator/ores.png) |
| Receta de flores | ![Flores](Images/Macerator/flores.png) |
| Procesando ore | ![Zinc](Images/Macerator/zinc.png) |

# Modificaciones hechas

## Recetas agregadas

- Procesamiento del brass
- Procesamiento del zinc
- Procesamiento de arenas, armaduras de caballo y de piedras del mod create que se quitaron de las máquinas eliminadas de ese mod

## Recetas modificadas

- Se modificaron varias recetas para que sean las mismas que las de create (por dar mayor items) y se eliminaron de ese mod

# Ejemplo de receta

## Procesar ores
```json
{
  "type": "modern_industrialization:macerator",
  "duration": 50,
  "eu": 2,
  "item_inputs": [
    {
      "amount": 1,
      "tag": "c:raw_gold_ores"
    }
  ],
  "item_outputs": [
    {
      "amount": 1,
      "item": "modern_industrialization:gold_dust"
    },
    {
      "amount": 1,
      "item": "modern_industrialization:gold_dust",
      "probability": 0.5
    }
  ]
}
```
[Fuente (23/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/generated/resources/data/modern_industrialization/recipes/materials/gold/macerator/raw_metal.json)

## Moler un item metálico
```json
{
  "type": "modern_industrialization:macerator",
  "duration": 100,
  "eu": 2,
  "item_inputs": [
    {
      "amount": 1,
      "item": "modern_industrialization:gold_large_plate"
    }
  ],
  "item_outputs": [
    {
      "amount": 4,
      "item": "modern_industrialization:gold_dust"
    }
  ]
}
```
[Fuente (23/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/generated/resources/data/modern_industrialization/recipes/materials/gold/macerator/large_plate.json)

## Cobblestone
```json
{
  "type": "modern_industrialization:macerator",
  "duration": 100,
  "eu": 2,
  "item_inputs": [
    {
      "amount": 1,
      "item": "minecraft:stone"
    }
  ],
  "item_outputs": [
    {
      "amount": 1,
      "item": "minecraft:cobblestone"
    }
  ]
}
```
[Fuente (23/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/generated/resources/data/modern_industrialization/recipes/vanilla_recipes/macerator/stone_to_cobblestone.json)