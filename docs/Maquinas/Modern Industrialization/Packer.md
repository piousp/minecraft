[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Packer >

![Interfaz en REI](Images/Packer/recetaREI.png)

Funciona como un empaquetador, agarra hasta 3 items diferente y produce 1 item.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Packer |
| Id del proceso | modern_industrialization:packer |
| Tiene interfaz | Si |
| Automatizable | Si |
| Acepta fluidos | No |
| Acepta items | Si, 3 campos |
| Produce fluidos | No |
| Produce items | Si, 1 campo |

# Cuando usarla

Si existe una receta `minecraft:shapeless` que use solo 3 items o menos debería meterse en el packer.

> ** NOTA**: El concepto clave es "empaquetar", todas las recetas del packer podrían hacerse en el [Assembler](Assembler.md), lo que define el lugar en donde va la receta es el concepto del proceso en si. Si la receta "empaqueta" y tiene 3 o menos tipos de items (tomando en cuenta el template, de ser usado) entonces se hace en el Packer, pero si el proceso "ensambla" entonces debería usarse el [Assembler](Assembler.md)

Hay que tener cuidado, porque si se mete una receta que usa 2 bricks y otras que usa 4 bricks entonces el packer no va a saber cual hacer, al menos que se bloqueé el output en la máquina.

Existen 2 templates, uno para recetas de 3x3 (para hacer bloques) y otro de 1x2 (en vertical), estos templates usan un espacio de item por lo que si se usa el template solo se podrían agregar recetas que usan 2 items.

Hay recetar de ensamblaje que por la cantidad de tipos de items podría hacerse en el Packer, sin embargo, al ver el contexto del item resultante se identifica que el proceso es más identificable como un ensamble que un empaque.

> **NOTA**: usar solo templates para eliminar ambigüedades y ponerle probabilidad 0 para que no sea consumido.

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Packer/Colocado.png) |
| Funcionando | ![Funcionando](Images/Packer/Funcionando.png) |
| Con template | ![Con template](Images/Packer/Con template.png) |
| Cables | ![Cables](Images/Packer/Cables.png) |

# Modificaciones hechas

## Recetas agregadas

- Recetas del manejo del zinc y brass, tanto polvos como lingotes
- Recetas del "Deploy" de create

# Ejemplo de receta

## Con template
```json
{
  "type": "modern_industrialization:packer",
  "duration": 100,
  "eu": 2,
  "item_inputs": [
    {
      "amount": 9,
      "tag": "c:electrum_ingots"
    },
    {
      "amount": 1,
      "item": "modern_industrialization:packer_block_template",
      "probability": 0.0
    }
  ],
  "item_outputs": [
    {
      "amount": 1,
      "item": "modern_industrialization:electrum_block"
    }
  ]
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/generated/resources/data/modern_industrialization/recipes/materials/electrum/packer/block.json)

## Cables
```json
{
  "type": "modern_industrialization:packer",
  "duration": 100,
  "eu": 2,
  "item_inputs": [
    {
      "amount": 6,
      "item": "modern_industrialization:rubber_sheet"
    },
    {
      "amount": 3,
      "item": "modern_industrialization:cupronickel_wire"
    }
  ],
  "item_outputs": [
    {
      "amount": 3,
      "item": "modern_industrialization:cupronickel_cable"
    }
  ]
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/generated/resources/data/modern_industrialization/recipes/materials/cupronickel/packer/cable.json)