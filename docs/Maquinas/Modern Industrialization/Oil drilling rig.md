[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Oil drilling rig >

![Interfaz en REI](Images/Oil drilling rig/recetaREI.png)

Es una máquina multibloque, parecido al funcionamiento del [Quarry](Quarry.md). Se obtiene petróleo o agua, según el drill que se use.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Oil drilling rig |
| Id del proceso | modern_industrialization:oil_drilling_rig |
| Tiene interfaz | Si |
| Automatizable | Si |
| Largo x Ancho x Altura | 5 x 5 x 10 <br> (estructura no cúbica) |

# Cuando usarla

- Para conseguir petróleo o agua

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Oil drilling rig/Colocado.png) |
| Funcionando | ![Funcionando](Images/Oil drilling rig/Funcionando.png) |

# Modificaciones hechas

No se han hecho modificaciones

# Ejemplo de receta

```json
{
  "type": "modern_industrialization:oil_drilling_rig",
  "eu": 32,
  "duration": 200,
  "item_inputs": {
    "item": "modern_industrialization:stainless_steel_drill",
    "amount": 1,
    "probability": 0.02
  },
  "fluid_outputs": {
    "fluid": "modern_industrialization:shale_oil",
    "amount": 4000
  }
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/main/resources/data/modern_industrialization/recipes/oil/shale_oil.json)
