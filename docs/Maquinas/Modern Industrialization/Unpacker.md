[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Unpacker >

![Interfaz en REI](Images/Unpacker/recetaREI.png)

Funciona como un desempaquetador, agarra 1 item y lo desempaqueta, tiene 2 campos de items de salida.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Unpacker |
| Id del proceso | modern_industrialization:unpacker |
| Tiene interfaz | Si |
| Automatizable | Si |
| Acepta fluidos | No |
| Acepta items | Si, 1 campo |
| Produce fluidos | No |
| Produce items | Si, 2 campos |

# Cuando usarla

Si existe una receta que de un item genere hasta 2 items entonces debería meterse en el unpacker.

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Unpacker/Colocado.png) |
| Funcionando | ![Funcionando](Images/Unpacker/Funcionando.png) |
| Nuggets | ![Nuggets](Images/Unpacker/Nuggets.png) |

# Modificaciones hechas

## Recetas agregadas

- Recetas del manejo del zinc y brass, tanto polvos como nuggets

# Ejemplo de receta

## Polvos
```json
{
  "type": "modern_industrialization:unpacker",
  "duration": 100,
  "eu": 2,
  "item_inputs": [
    {
      "amount": 1,
      "tag": "c:bronze_dusts"
    }
  ],
  "item_outputs": [
    {
      "amount": 9,
      "item": "modern_industrialization:bronze_tiny_dust"
    }
  ]
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/generated/resources/data/modern_industrialization/recipes/materials/bronze/unpacker/tiny_dust.json)

## Hatch
```json
{
  "type": "modern_industrialization:unpacker",
  "duration": 200,
  "eu": 2,
  "item_inputs": [
    {
      "amount": 1,
      "item": "modern_industrialization:mv_energy_output_hatch"
    }
  ],
  "item_outputs": [
    {
      "amount": 1,
      "item": "modern_industrialization:advanced_machine_hull"
    },
    {
      "amount": 1,
      "item": "modern_industrialization:electrum_cable"
    }
  ]
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/generated/resources/data/modern_industrialization/recipes/hatches/advanced/unpacker/energy_output_hatch.json)