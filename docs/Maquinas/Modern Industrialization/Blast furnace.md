[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Blast furnace >

![Interfaz en REI](Images/Blast furnace/recetaREI.png)

Es una máquina multibloque que se construye para empezar a cocinar el *steel dust*. Tiene 3 tiers, el primero usa steam, los otros son eléctricos. El blast furnace de minecraft no tiene nada que ver con este, son completamente independientes en recetas.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Blast furnace |
| Id del proceso | modern_industrialization:blast_furnace |
| Tiene interfaz | Si |
| Automatizable | Si |
| Largo x Ancho x Altura | 3 x 3 x 4 |

# Cuando usarla

- Para obtener los lingotes más avanzados
- Para procesar los minerales radioactivos

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Blast furnace/Colocado.png) |
| Funcionando | ![Funcionando](Images/Blast furnace/Funcionando.png) |
| Lingote caliente | ![Lingote caliente](Images/Blast furnace/Lingote caliente.png) |
| Uranio | ![Uranio](Images/Blast furnace/Uranio.png) |
| Fundir item | ![Fundir item](Images/Blast furnace/Fundir item.png) |

# Modificaciones hechas

No se han hecho modificaciones

# Ejemplo de receta

```json
{
  "type" : "modern_industrialization:blast_furnace",
  "eu" : 4,
  "duration" : 300,
  "item_inputs" : {
    "tag" : "c:soldering_alloy_dusts",
    "amount" : 1
  },
  "fluid_outputs" : {
    "fluid" : "modern_industrialization:soldering_alloy",
    "amount" : 111.11
  }
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/main/resources/data/modern_industrialization/recipes/materials/blast_furnace/soldering_alloy.json)
