[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Cutting machine >

![Interfaz en REI](Images/Cutting machine/recetaRei.png)

Esta es una de las máquinas básicas, funciona como un cortador industrial.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Cutting machine |
| Id del proceso | modern_industrialization:cutting_machine |
| Tiene interfaz | Si |
| Automatizable | Si |
| Acepta fluidos | Si, 1 campo |
| Acepta items | Si, 1 campo |
| Produce fluidos | No |
| Produce items | Si, 1 campo |

# Cuando usarla

Reemplaza al [Forge hammer](Forge hammer.md), se usa para:

- **Cortar metales** para hacer los rods y bolts
- **Cortar items** para crear items tales como paneles de vidrio o alfombras.
- **Wood cutting** Se pueden hacer las mismas recetas que el wood cutter.
- **Stone cutting** Se pueden hacer las mismas recetas que el stone cutter.

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Cutting machine/colocado.png) |
| Receta de alfombras | ![Alfombras](Images/Cutting machine/alfombras.png) |
| Cortando cobre | ![Copper rod](Images/Cutting machine/rod.png) |

# Modificaciones hechas

No se han modificado recetas

# Ejemplo de receta

## Bolts
```json
{
  "type": "modern_industrialization:cutting_machine",
  "duration": 200,
  "eu": 2,
  "fluid_inputs": [
    {
      "amount": 1,
      "fluid": "modern_industrialization:lubricant"
    }
  ],
  "item_inputs": [
    {
      "amount": 1,
      "item": "modern_industrialization:aluminum_rod"
    }
  ],
  "item_outputs": [
    {
      "amount": 2,
      "item": "modern_industrialization:aluminum_bolt"
    }
  ]
}
```
[Fuente (23/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/generated/resources/data/modern_industrialization/recipes/materials/aluminum/cutting_machine/rod.json)

## Paneles de vidrio
```json
{
  "type": "modern_industrialization:cutting_machine",
  "eu": 2,
  "duration": 100,
  "item_inputs": {
    "item": "minecraft:glass",
    "amount": 6
  },
  "fluid_inputs": {
    "fluid": "modern_industrialization:lubricant",
    "amount": 1
  },
  "item_outputs": {
    "item": "minecraft:glass_pane",
    "amount": 16
  }
}
```
[Fuente (23/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/main/resources/data/modern_industrialization/recipes/vanilla_recipes/cutting_machine/glass_panes.json)