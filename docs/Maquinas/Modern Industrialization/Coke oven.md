[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Coke oven >

![Interfaz en REI](Images/Coke oven/recetaREI.png)

Es la primer máquina multibloque que se construye en la etapa de máquinas de bronce. Solo funciona con steam. Puede usarse para automatizar procesos.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Coke oven |
| Id del proceso | modern_industrialization:coke_oven |
| Tiene interfaz | Si |
| Automatizable | Si |
| Largo x Ancho x Altura | 3 x 3 x 3 |

# Cuando usarla

Solo es usado para generar coke

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Coke oven/Colocado.png) |
| Funcionando | ![Funcionando](Images/Coke oven/Funcionando.png) |

# Modificaciones hechas

No se han hecho modificaciones

# Ejemplo de receta

```json
{
  "type" : "modern_industrialization:coke_oven",
  "eu" : 2,
  "duration" : 600,
  "item_inputs" : {
    "item" : "minecraft:coal",
    "amount" : 1
  },
  "fluid_outputs" : {
      "fluid" : "modern_industrialization:creosote",
      "amount" : 500,
      "probability" : 0.5
  },
  "item_outputs" : {
    "item" : "modern_industrialization:coke",
    "amount" : 1
  }
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/main/resources/data/modern_industrialization/recipes/materials/coke_oven/coke.json)
