[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Electrolizer >

![Interfaz en REI](Images/Electrolizer/recetaREI.png)

Es una máquina avanzada que realiza electrólisis.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Electrolizer |
| Id del proceso | modern_industrialization:electrolyzer |
| Tiene interfaz | Si |
| Automatizable | Si |
| Acepta fluidos | Si, 1 campo |
| Acepta items | Si, 1 campo |
| Produce fluidos | Si, 4 campos |
| Produce items | Si, 4 campos |

# Cuando usarla

- Para hacer electrólisis a items o fluidos

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Electrolizer/Colocado.png) |
| Funcionando | ![Funcionando](Images/Electrolizer/Funcionando.png) |
| Líquidos | ![Líquidos](Images/Electrolizer/Líquidos.png) |

# Modificaciones hechas

No se han hecho modificaciones

# Ejemplo de receta

## Líquido
```json
{
  "type": "modern_industrialization:electrolyzer",
  "eu": 8,
  "duration": 600,
  "fluid_inputs": {
    "fluid": "minecraft:water",
    "amount": 3000
  },
  "fluid_outputs": [
    {
      "fluid": "modern_industrialization:hydrogen",
      "amount": 2000
    },
    {
      "fluid": "modern_industrialization:oxygen",
      "amount": 1000
    }
  ]
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/main/resources/data/modern_industrialization/recipes/materials/electrolyzer/water.json)

## Item

```json
{
  "type": "modern_industrialization:electrolyzer",
  "eu": 32,
  "duration": 2400,
  "item_inputs": {
    "tag": "c:lapis_dusts",
    "amount": 18
  },
  "item_outputs": [
    {
      "item": "modern_industrialization:aluminum_dust",
      "amount": 3
    },
    {
      "item": "modern_industrialization:sodium_dust",
      "amount": 2
    },
    {
      "item": "modern_industrialization:silicon_dust",
      "amount": 1
    }
  ],
  "fluid_outputs": {
    "fluid": "modern_industrialization:oxygen",
    "amount": 2500,
    "probability": 0.5
  }
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/main/resources/data/modern_industrialization/recipes/materials/electrolyzer/lapis.json)