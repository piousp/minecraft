[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Distillation tower >

![Interfaz en REI](Images/Distillation tower/recetaREI.png)

Es una máquina multibloque, es la versión mejorada del [Distillery](Distillery.md) procesa el petróleo (y un deribado de este)

| | Valor |
| --- | ----------- |
| Nombre del proceso | Distillation tower |
| Id del proceso | modern_industrialization:distillation_tower |
| Tiene interfaz | Si |
| Automatizable | Si |
| Largo x Ancho x Altura | 3 x 3 x 1 + n <br> (n es la cantidad de derivados a obtener, 1 <= n <= 9) |

# Cuando usarla

- Para destilar líquidos

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Distillation tower/Colocado.png) |
| Funcionando | ![Funcionando](Images/Distillation tower/Funcionando.png) |
| Mínima | ![Funcionando](Images/Distillation tower/Mínima.png) |
| Máxima | ![Funcionando](Images/Distillation tower/Máxima.png) |

# Modificaciones hechas

No se han hecho modificaciones

# Ejemplo de receta

```json
{
  "type": "modern_industrialization:distillation_tower",
  "duration": 200,
  "eu": 120,
  "fluid_inputs": [
    {
      "amount": 1000,
      "fluid": "modern_industrialization:steam_cracked_naphtha"
    }
  ],
  "fluid_outputs": [
    {
      "amount": 150,
      "fluid": "modern_industrialization:methane"
    },
    {
      "amount": 50,
      "fluid": "modern_industrialization:acetylene"
    },
    {
      "amount": 250,
      "fluid": "modern_industrialization:ethylene"
    },
    {
      "amount": 75,
      "fluid": "modern_industrialization:propene"
    },
    {
      "amount": 125,
      "fluid": "modern_industrialization:butadiene"
    },
    {
      "amount": 150,
      "fluid": "modern_industrialization:benzene"
    },
    {
      "amount": 100,
      "fluid": "modern_industrialization:toluene"
    },
    {
      "amount": 100,
      "fluid": "modern_industrialization:ethylbenzene"
    }
  ]
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/generated/resources/data/modern_industrialization/recipes/petrochem/distillation/steam_cracked_naphtha_full.json)
