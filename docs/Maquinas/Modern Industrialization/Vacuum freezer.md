[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Vacuum freezer >

![Interfaz en REI](Images/Vacuum freezer/recetaREI.png)

Es una máquina multibloque que se construye para empezar a enfriar los *hot ingots* de minerales avanzados que salen del [Blast Furnace](Blast Furnace.md).

| | Valor |
| --- | ----------- |
| Nombre del proceso | Vacuum freezer |
| Id del proceso | modern_industrialization:vacuum_freezer |
| Tiene interfaz | Si |
| Automatizable | Si |
| Largo x Ancho x Altura | 3 x 3 x 4 |

# Cuando usarla

- Para enfriar algún item o líquido
- También es usado para generar aire líquido

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Vacuum freezer/Colocado.png) |
| Funcionando | ![Funcionando](Images/Vacuum freezer/Funcionando.png) |
| Aire líquido | ![Aire líquido](Images/Vacuum freezer/Aire líquido.png) |
| Obsidian | ![Obsidian](Images/Vacuum freezer/Obsidian.png) |

# Modificaciones hechas

Se metió la receta de enfriar el chocolate líquido en barras de chocolate

# Ejemplo de receta

```json
{
  "type": "modern_industrialization:vacuum_freezer",
  "eu": 32,
  "duration" : 200,

  "item_inputs": {
    "item" : "modern_industrialization:air_intake",
    "probability": 0
  },

  "fluid_outputs": {
    "fluid" : "modern_industrialization:liquid_air",
    "amount" : 1000
  }

}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/main/resources/data/modern_industrialization/recipes/materials/vacuum_freezer/liquid_air.json)
