[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Heat exchanger >

![Interfaz en REI](Images/Heat exchanger/recetaREI.png)

Es una máquina multibloque que utiliza las leyes de la termodinámica que transferir el calor de un item/líquido a otro. Intercambiar el calor es más eficiente que usar el [Vacuum freezer](Vacuum freezer.md)

| | Valor |
| --- | ----------- |
| Nombre del proceso | Heat exchanger |
| Id del proceso | modern_industrialization:heat_exchanger |
| Tiene interfaz | Si |
| Automatizable | Si |
| Largo x Ancho x Altura | 3 x 5 x 3 |

# Cuando usarla

- Cuando hay que enfriar algo de forma rápida

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Heat exchanger/Colocado.png) |
| Colocado otra cara | ![Colocado2](Images/Heat exchanger/Colocado2.png) |
| Funcionando | ![Funcionando](Images/Heat exchanger/Funcionando.png) |

# Modificaciones hechas

No se han hecho modificaciones

# Ejemplo de receta

```json
{
  "type": "modern_industrialization:heat_exchanger",
  "duration": 10,
  "eu": 8,
  "fluid_inputs": [
    {
      "amount": 100,
      "fluid": "modern_industrialization:cryofluid"
    }
  ],
  "fluid_outputs": [
    {
      "amount": 65,
      "fluid": "modern_industrialization:argon"
    },
    {
      "amount": 25,
      "fluid": "modern_industrialization:helium"
    }
  ],
  "item_inputs": [
    {
      "amount": 1,
      "item": "modern_industrialization:chromium_hot_ingot"
    }
  ],
  "item_outputs": [
    {
      "amount": 1,
      "item": "modern_industrialization:chromium_ingot"
    }
  ]
}
```
[Fuente (25/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/generated/resources/data/modern_industrialization/recipes/materials/chromium/heat_exchanger/hot_ingot.json)
