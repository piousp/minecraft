[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Fusion reactor >

![Interfaz en REI](Images/Fusion reactor/recetaREI.png)

Es una máquina multibloque, fusiona isótopos de hidrógeno

| | Valor |
| --- | ----------- |
| Nombre del proceso | Fusion reactor |
| Id del proceso | modern_industrialization:fusion_reactor |
| Tiene interfaz | Si |
| Automatizable | Si |
| Radio x Altura | Un torus de diámetro 15 |

# Cuando usarla

> **Opinión**: Aún no se como se usa, nunca lo he construido. Según receta usa 16k EU per tick

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Fusion reactor/Colocado.png) |

# Modificaciones hechas

Se desconoce el id del proceso

# Ejemplo de receta

```json
{
  "type": "modern_industrialization:fusion_reactor",
  "eu": 16000,
  "duration": 1200,
  "fluid_inputs": [
    {
      "fluid": "modern_industrialization:deuterium",
      "amount": 1000
    },
    {
      "fluid": "modern_industrialization:deuterium",
      "amount": 1000
    }
  ],
  "fluid_outputs": [
    {
      "fluid": "modern_industrialization:helium_plasma",
      "amount": 500
    },
    {
      "fluid": "modern_industrialization:tritium",
      "amount": 500
    }
  ]
}
```
[Fuente (25/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/main/resources/data/modern_industrialization/recipes/fusion_reactor/deuterium_deuterium.json)
