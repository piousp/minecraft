[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Implosion compressor >

![Interfaz en REI](Images/Implosion compressor/recetaREI.png)

Es una máquina multibloque que utiliza explosiones para lograr una mayor presión de compresión para crear items.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Implosion compressor |
| Id del proceso | modern_industrialization:implosion_compressor |
| Tiene interfaz | Si |
| Automatizable | Si |
| Largo x Ancho x Altura | 3 x 3 x 4 |

# Cuando usarla

- Para crear items que solo se pueden crear a grandes presiones

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Implosion compressor/Colocado.png) |
| Funcionando | ![Funcionando](Images/Implosion compressor/Funcionando.png) |
| Diamante | ![Diamante](Images/Implosion compressor/Diamante.png) |

# Modificaciones hechas

Se metió una receta de create

# Ejemplo de receta

```json
{
  "type": "modern_industrialization:implosion_compressor",
  "eu": 1,
  "duration": 10,
  "item_inputs": [
    {
      "item": "modern_industrialization:mixed_ingot_blastproof",
      "amount": 1
    },
    {
      "item": "modern_industrialization:industrial_tnt",
      "amount": 1
    }
  ],
  "item_outputs": {
    "item": "modern_industrialization:blastproof_alloy_ingot",
    "amount": 3
  }
}
```
[Fuente (25/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/main/resources/data/modern_industrialization/recipes/materials/implosion_compressor/blastproof_alloy_plate.json)
