[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Nuclear reactor >

Es una máquina multibloque, procesa combustible radioactivo

| | Valor |
| --- | ----------- |
| Nombre del proceso | Nuclear reactor |
| Id del proceso | |
| Tiene interfaz | Si |
| Automatizable | Si |
| Radio x Altura | (3, 4, 5, 6) x 5 <br> cilindro de radio variante |

# Cuando usarla

> **Opinión**: Aún no se como se usa

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Nuclear reactor/Colocado.png) |

# Modificaciones hechas

Se desconoce el id del proceso
