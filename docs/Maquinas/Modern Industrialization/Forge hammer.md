[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Forge hammer >

![Interfaz en REI](Images/Forge hammer/recetaRei.png)

Este es una máquina que realiza los procesamientos iniciales del mod, es lo primero que se construye, hace todo a masazos. Su uso es completamente manual.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Forge hammer |
| Id del proceso | modern_industrialization:forge_hammer |
| Tiene interfaz | Si |
| Automatizable | No |
| Acepta fluidos | No |
| Acepta items | Si, 1 campo |
| Produce fluidos | No |
| Produce items | Si, 1 campo |

# Cuando usarla

Es la primer máquina que se debe de constrir, sus usos son los siquiente:

- **Procesar los ores** en el metal en bruto o en los polvos.
- **Procesar los lingotes** para crear rods, bolts, plates, rings y curved plates.

Se deben de incorporar las recetas aquí cuando hay minerales nuevos de acceso inicial para su procesamiento. Las únicas aleaciones que son procesadas aquí son el bronce y el acero. El carbón es el único mineral no metálico que es procesado.

Las siguientes máquinas reeemplazan su uso:

- **[Macerator](Macerator.md)**: para el procesamiento de los ores
- **[Cutting machine](Cutting machine.md)**: para crear los rods y bolts
- **[Compressor](Compressor.md)**: para crear los plates, curved plates y los rings

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Forge hammer/Colocado.png) |
| Sin herramienta | ![Sin herramienta](Images/Forge hammer/Sin herramienta.png) |
| Con herramienta | ![Con herramienta](Images/Forge hammer/Con herramienta.png) |

# Modificaciones hechas

Se agregó el procesamiento del zinc

# Ejemplo de receta
El valor de *eu* es el que determina si se usa la herramienta o no

## Sin herramienta
```json
{
  "type": "modern_industrialization:forge_hammer",
  "duration": 0,
  "eu": 0,
  "item_inputs": [
    {
      "amount": 1,
      "item": "modern_industrialization:gold_double_ingot"
    }
  ],
  "item_outputs": [
    {
      "amount": 1,
      "item": "modern_industrialization:gold_plate"
    }
  ]
}
```
[Fuente (23/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/generated/resources/data/modern_industrialization/recipes/materials/gold/forge_hammer/double_ingot_to_plate.json)

## Con herramienta
```json
{
  "type": "modern_industrialization:forge_hammer",
  "duration": 0,
  "eu": 0,
  "item_inputs": [
    {
      "amount": 1,
      "item": "modern_industrialization:gold_double_ingot"
    }
  ],
  "item_outputs": [
    {
      "amount": 1,
      "item": "modern_industrialization:gold_plate"
    }
  ]
}
```
[Fuente (23/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/generated/resources/data/modern_industrialization/recipes/materials/gold/forge_hammer/ingot_to_ring_with_tool.json)