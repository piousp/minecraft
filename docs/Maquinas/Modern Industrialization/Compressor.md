[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Compressor >

![Interfaz en REI](Images/Compressor/recetaRei.png)

Esta es una de las máquinas básicas, funciona como una prensa hidráulica para trabajar metales.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Compressor |
| Id del proceso | modern_industrialization:compressor |
| Tiene interfaz | Si |
| Automatizable | Si |
| Acepta fluidos | No |
| Acepta items | Si, 1 campo |
| Produce fluidos | No |
| Produce items | Si, 1 campo |

# Cuando usarla

Reemplaza al [Forge hammer](Forge hammer.md), se usa para:

- **Aplastar** lingotes para obtener plates
- **Doblar** plates y rods para obtener curved plates y rings
- **Comprimir** comprimir los polvos, por ejemplo, coal dust para obtener coal

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Compressor/colocado.png) |
| Receta de curved plate | ![Alfombras](Images/Compressor/curved plate.png) |
| Haciendo un ring | ![ring](Images/Compressor/ring.png) |

# Modificaciones hechas

Solo se agregó poder hacer brass sheets.

# Ejemplo de receta

## Plate
```json
{
  "type": "modern_industrialization:compressor",
  "duration": 100,
  "eu": 2,
  "item_inputs": [
    {
      "amount": 1,
      "tag": "c:cupronickel_ingots"
    }
  ],
  "item_outputs": [
    {
      "amount": 1,
      "item": "modern_industrialization:cupronickel_plate"
    }
  ]
}
```
[Fuente (23/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/generated/resources/data/modern_industrialization/recipes/materials/cupronickel/compressor/main.json)

## Comprimir
```json
{
  "type": "modern_industrialization:compressor",
  "duration": 100,
  "eu": 2,
  "item_inputs": [
    {
      "amount": 1,
      "tag": "c:coal_dusts"
    }
  ],
  "item_outputs": [
    {
      "amount": 1,
      "item": "minecraft:coal"
    }
  ]
}
```
[Fuente (23/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/generated/resources/data/modern_industrialization/recipes/materials/coal/compressor/coal.json)