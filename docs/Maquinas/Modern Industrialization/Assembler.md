[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Assembler >

![Interfaz en REI](Images/Assembler/recetaREI.png)

Es una máquina avanzada que se utiliza para ensamblar componentes. Se podría considerar una versión más avanzada del [Packer](Packer.md), pero se diferencia de este por el concepto de que el [Packer](Packer.md) empaqueta mientras que el Assembler ensambla.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Assembler |
| Id del proceso | modern_industrialization:assembler |
| Tiene interfaz | Si |
| Automatizable | Si |
| Acepta fluidos | Si, 2 campos |
| Acepta items | Si, 9 campos |
| Produce fluidos | No |
| Produce items | Si, 3 campos |

# Cuando usarla

- El ensamblaje produce un solo tipo de item (por concepto, no por restricción)
- **Armar items** que no entran en la definición de "empaquetar", 
- **Crear items** con muchas cantidad de items. Las recetas de `crafting table` solo pueden tener 9 items en total, sin embargo, el assembler puede hacer recetas con 9 **tipo** de items 

> ** NOTA**: El concepto clave es "ensamblar", todas las recetas del [Packer](Packer.md) podrían hacerse en el Assembler, lo que define el lugar en donde va la receta es el concepto del proceso en si. Si la receta "empaqueta" y tiene 3 o menos tipos de items (tomando en cuenta el template, de ser usado) entonces se hace en el [Packer](Packer.md), pero si el proceso "ensambla" entonces debería usarse el Assembler.

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Assembler/Colocado.png) |
| Funcionando | ![Funcionando](Images/Assembler/Funcionando.png) |
| Muchas items | ![Muchas items](Images/Assembler/Muchas items.png) |

# Modificaciones hechas

Se agregaron recetas que vienen del "Recipe sequence", "deployer" y "mechanical crafter" de create.

# Ejemplo de receta

## Hatch
```json
{
  "type": "modern_industrialization:assembler",
  "duration": 200,
  "eu": 8,
  "item_inputs": [
    {
      "amount": 1,
      "item": "modern_industrialization:advanced_machine_hull"
    },
    {
      "amount": 1,
      "item": "modern_industrialization:aluminum_tank"
    }
  ],
  "item_outputs": [
    {
      "amount": 1,
      "item": "modern_industrialization:advanced_fluid_output_hatch"
    }
  ]
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/generated/resources/data/modern_industrialization/recipes/materials/steel/polarizer/rod_magnetic.json)

## Muchos items

```json
{
  "type": "modern_industrialization:assembler",
  "duration": 200,
  "eu": 16,
  "fluid_inputs": [
    {
      "amount": 500,
      "fluid": "modern_industrialization:soldering_alloy"
    },
    {
      "amount": 100,
      "fluid": "modern_industrialization:helium"
    }
  ],
  "item_inputs": [
    {
      "amount": 2,
      "item": "modern_industrialization:blastproof_alloy_curved_plate"
    },
    {
      "amount": 1,
      "item": "modern_industrialization:large_motor"
    },
    {
      "amount": 2,
      "item": "modern_industrialization:robot_arm"
    },
    {
      "amount": 18,
      "item": "modern_industrialization:le_mox_rod"
    }
  ],
  "item_outputs": [
    {
      "amount": 1,
      "item": "modern_industrialization:le_mox_fuel_rod"
    }
  ]
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/generated/resources/data/modern_industrialization/recipes/materials/le_mox/assembler/fuel_rod.json)