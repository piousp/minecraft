[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Distillery >

![Interfaz en REI](Images/Distillery/recetaREI.png)

Es una máquina avanzada que destila líquidos. Este es la versión simple del [Distillation tower](Distillation tower.md) porque solo se obtiene 1 líquido en el proceso.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Distillery |
| Id del proceso | modern_industrialization:distillery |
| Tiene interfaz | Si |
| Automatizable | Si |
| Acepta fluidos | Si, 1 campo |
| Acepta items | No |
| Produce fluidos | Si, 1 campo |
| Produce items | No |

# Cuando usarla

Cuando hay que destilar líquidos, por lo general combustibles líquidos.

> **NOTA**: por lo general el proceso de destilado produce varios líquidos, el Distillery solo puede producir uno, si se quieren obtener todos los derivados posible hay que utilizar el [Distillation tower](Distillation tower.md)

# Imágenes

| | Imagen |
| --- | :---: |
| Colocado en el mundo | ![Colocado](Images/Distillery/Colocado.png) |
| Funcionando | ![Funcionando](Images/Distillery/Funcionando.png) |

# Modificaciones hechas

No se han hecho modificaciones

# Ejemplo de receta

```json
{
  "type": "modern_industrialization:distillery",
  "duration": 800,
  "eu": 8,
  "fluid_inputs": [
    {
      "amount": 1000,
      "fluid": "modern_industrialization:sugar_solution"
    }
  ],
  "fluid_outputs": [
    {
      "amount": 10,
      "fluid": "modern_industrialization:ethanol"
    }
  ]
}
```
[Fuente (24/mar/23)](https://github.com/AztechMC/Modern-Industrialization/blob/master/src/main/resources/data/modern_industrialization/recipes/oil/distillation/ethanol_from_sugar.json)
