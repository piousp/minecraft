[Home](../../README.md) > Machote >

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Lorem ipsum |
| Id del proceso | Lorem:ipsum |
| Tiene interfaz | No |
| Acepta fluidos | No |
| Acepta items | No |
| Produce fluidos | No |
| Produce items | No |

# Cuando usarla

Lorem ipsum:

- **Lorem ipsum**: Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

# Imágenes

| | Imagen |
| --- | :---: |
| Lorem ipsum | ![markdown](https://d33wubrfki0l68.cloudfront.net/f1f475a6fda1c2c4be4cac04033db5c3293032b4/513a4/assets/images/markdown-mark-white.svg) |

# Modificaciones hechas

## Recetas agregadas

- Lorem ipsum

## Recetas eliminadas

- Lorem ipsum

# Ejemplo de receta

```json
{
  "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
}
```
[Fuente (23/mar/23)](https://www.lipsum.com/)
