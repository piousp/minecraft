[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Bulk smoking >

![Interfaz en REI](images/Bulk smoking/recetaRei.png)

Este es una máquina multibloque que usa el humo del fuego para cocinar. El fuego puede ser proveido por un *campfire* encendido o por fuego sobre *netherrack*.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Bulk smoking |
| Tiene interfaz | No |
| Acepta fluidos | No |
| Acepta items | Si |
| Produce fluidos | No |
| Produce items | Si |

# Cuando usarla

Es un máquina más que procesa todas las recetas de `minecraft:smoking`, cualquier receta que se meta al proceso de smoking podrá ser procesada por esta máquina.

# Imágenes

| | Imagen |
| --- | :---: |
| Fuegos | ![fuegos](images/Bulk smoking/fuegos.png) |
| Items procesados | ![procesamiento](images/Bulk smoking/procesamiento.png) |

# Modificaciones hechas

No es han hecho modificaciones
