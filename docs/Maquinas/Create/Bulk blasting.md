[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Bulk blasting >

![Interfaz en REI](images/Bulk blasting/recetaRei.png)

Este es una máquina multibloque que usa el viento calentado por lava. El viento tiene que soplar a travez de lava (no es necesario que sea una fuente).

| | Valor |
| --- | ----------- |
| Nombre del proceso | Bulk blasting |
| Tiene interfaz | No |
| Acepta fluidos | No |
| Acepta items | Si |
| Produce fluidos | No |
| Produce items | Si |

# Cuando usarla

Es un máquina más que procesa todas las recetas de `minecraft:blasting`, cualquier receta que se meta al proceso de blasting podrá ser procesada por esta máquina.

# Imágenes

| | Imagen |
| --- | :---: |
| Estructura | ![lava](images/Bulk blasting/lava.png) |
| Items procesados | ![procesamiento](images/Bulk blasting/procesamiento.png) |

# Modificaciones hechas

No es han hecho modificaciones