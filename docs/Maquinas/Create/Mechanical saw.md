[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Mechanical saw >

![Interfaz en REI](images/Mechanical saw/recetaRei.png)

Este es una máquina que tiene la misma funcionalidad que el stonecutting y woodcutting vainilla.

| | Valor |
| --- | ----------- |
| Nombre del proceso para madera | Sawing |
| Nombre del proceso para piedra | Block cutting |
| Id del proceso | create:cutting |
| Tiene interfaz | No |
| Acepta fluidos | No |
| Acepta items | Si |
| Produce fluidos | No |
| Produce items | Si |

# Cuando usarla

> **Opinión**: Aún no se como se usa, no ve razón de usar esta máquina por encima de las vainillas.

# Imágenes

Sin imágenes

# Modificaciones hechas

No es han hecho modificaciones

# Ejemplo de receta

```json
{
  "type": "create:cutting",
  "ingredients": [
    {
      "item": "minecraft:dark_oak_log"
    }
  ],
  "processingTime": 50,
  "results": [
    {
      "item": "minecraft:stripped_dark_oak_log"
    }
  ]
}
```