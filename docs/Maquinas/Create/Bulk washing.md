[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Bulk washing >

![Interfaz en REI](images/Bulk washing/recetaRei.png)

Este es una máquina multibloque que usa viento húmedo para procesar items. El viento tiene que soplar a travez de agua (no es necesario que sea una fuente).

| | Valor |
| --- | ----------- |
| Nombre del proceso | Bulk washing |
| Id del proceso | create:splashing |
| Tiene interfaz | No |
| Acepta fluidos | No |
| Acepta items | Si |
| Produce fluidos | No |
| Produce items | Si |

# Cuando usarla

Se recomienda el uso para recetas que requieran:

- **Lavar**: items que tienen sus variantes en alguno o todos los 16 colores que soporte el juego y se requiere una receta que les quite el color. Se recomienda que los items con color comparten un tag para facilitar la receta.
- **Empapar**: items que al mojarse en exceso cambian, por ejemplo, cuando se moja tierra se obtiene barro. También aplica enfriar con agua.
- **Limpiar**: Procesos que requieren limpieza, por ejemplo, un mezcla de azúcar y piedras, con agua disuelvo el azúcar y me queda las piedras. Posiblemente el [Mixer](../Modern Industrialization/Mixer.md) sea más apto para este proceso

# Imágenes

| | Imagen |
| --- | :---: |
| Uso Manual | ![Simple](images/Bulk washing/Simple.png) |
| Item a procesar | ![wheat flour](images/Bulk washing/wheat flour.png) |
| Item procesado | ![dough](images/Bulk washing/dough.png) |
| Uso automático | ![Automatica](images/Bulk washing/Automatica.png) |

# Modificaciones hechas

## Recetas agregadas

- Lavar los cables de AE<sup>2</sup>
- Empapar arena y tierra (Recetas migradas del Mixer)

## Recetas eliminadas

- Limpiar los crushed ores que trae el mod, todo el procesamiento de minerales se hace con `Modern Industrialization`
- Las recetas que traía para los 2 tipos de arena, se cambia por la que se agregó

# Ejemplo de receta

## Quitar el color

```json
{
  "type": "create:splashing",
  "ingredients": [
    {
      "tag": "minecraft:wool"
    }
  ],
  "results": [
    {
      "item": "minecraft:white_wool"
    }
  ]
}
````
[Fuente (23/mar/23)](https://github.com/Creators-of-Create/Create/blob/mc1.19/dev/src/generated/resources/data/create/recipes/splashing/wool.json)

## Empapar item

```json
{
  "type": "create:splashing",
  "ingredients": [
    {
      "tag": "forge:flour/wheat"
    }
  ],
  "results": [
    {
      "item": "create:dough"
    }
  ]
}
```
[Fuente (23/mar/23)](https://github.com/Creators-of-Create/Create/blob/mc1.19/dev/src/generated/resources/data/create/recipes/splashing/wheat_flour.json)

## Limpiar item

```json
{
  "type": "create:splashing",
  "ingredients": [
    {
      "item": "minecraft:gravel"
    }
  ],
  "results": [
    {
      "chance": 0.25,
      "item": "minecraft:flint"
    },
    {
      "chance": 0.125,
      "item": "minecraft:iron_nugget"
    }
  ]
}
```
[Fuente (23/mar/23)](https://github.com/Creators-of-Create/Create/blob/mc1.19/dev/src/generated/resources/data/create/recipes/splashing/gravel.json)