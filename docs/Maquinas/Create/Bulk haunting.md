[Home](../../../README.md) > [Lista de máquinas](../../Lista maquinas.md) > Bulk haunting >

![Interfaz en REI](images/Bulk haunting/recetaRei.png)

Este es una máquina multibloque que usa el humo del fuego. El fuego puede ser proveido por un *soul campfire* encendido.

| | Valor |
| --- | ----------- |
| Nombre del proceso | Bulk haunting |
| Id del proceso | create:haunting |
| Tiene interfaz | No |
| Acepta fluidos | No |
| Acepta items | Si |
| Produce fluidos | No |
| Produce items | Si |

# Cuando usarla

Es utilizada para:

- **Maldecir items**: por ejemplo crear *infected stone* del *stone*
- **"Infernalizar" items**: convertir *sand* en *soul sand* o convertir *brick* en *nether brick*

# Imágenes

| | Imagen |
| --- | :---: |
| Fogata | ![fogata](images/Bulk haunting/fogata.png) |
| Items procesados | ![procesamiento](images/Bulk haunting/procesamiento.png) |

# Modificaciones hechas

No es han hecho modificaciones

# Ejemplo de receta

## Maldecir

```json
{
  "type": "create:haunting",
  "ingredients": [
    {
      "item": "minecraft:stone"
    }
  ],
  "results": [
    {
      "item": "minecraft:infested_stone"
    }
  ]
}
````
[Fuente (23/mar/23)](https://github.com/Creators-of-Create/Create/blob/mc1.19/dev/src/generated/resources/data/create/recipes/haunting/infested_stone.json)

## Infernalizar item

```json
{
  "type": "create:haunting",
  "ingredients": [
    {
      "item": "minecraft:brick"
    }
  ],
  "results": [
    {
      "item": "minecraft:nether_brick"
    }
  ]
}
```
[Fuente (23/mar/23)](https://github.com/Creators-of-Create/Create/blob/mc1.19/dev/src/generated/resources/data/create/recipes/haunting/nether_brick.json)