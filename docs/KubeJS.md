[Home](../README.md) > KubeJS >

# Scripts
**KubeJS** utiliza 3 carpetas donde van los scripts:

- `startup_scripts` va a contener los que se corren cuando se ejecuta minecraft.
- `server_scripts` va a contener los scripts que se corren cuando se inicio el servidor (o se carga el mundo en local), aquí se meten las modificaciones a las recetas.
- `client_scripts` va a contener los scripts que se ejecutan en el cliente, aquí se meten las modificaciones a JEI/REI.

## Server scripts
Dentro de esta carpeta existe el archivo default `script.js` se puede modificar para hacer pruebas rápidas pero no recomiendo utilizarlo, para eso mejor utilizar la jerarquía de carpetas y archivos propuesta a continuanción.

### Carpetas iniciales
![Carpetas iniciales](images/CarpetasIniciales.png)

En la carpea `Recipes` se van a meter todas las recetas que se están agregando, modificando o eliminando. En `Tags` se van a meter todos los items a los cuales se les están agrando, modificando o eliminando los tags (ej: `#minecraft:stones`).

También está el archivo `script.js` que es el archivo default (no es necesario que esté presente).

![Carpetas de operaciones](images/CarpetasOperacion.png)

Dentro de estas carpetas se van a crear 3 carpetas según la operación:

- Agregar
- Eliminar
- Modificar

### Estructura de las carpetas de operaciones

![Estructura de las carpetas de operaciones](images/EstructuraOperacion.png)

Dentro de cada una de esas carpetas tenemos 2 opciones para poner nuestros scripts:

1. Crear una carpeta por `mod` a trabajar con las recetas.
3. Crear un archivo con el formato `<mod>.js` para realizar pequeñas modificaciones, si el archivo empieza a contener muchas modificaciones debería ser convertido a la primer opción.

Con la opción 1 ya se puede tener más libertad en la estructura interna, esta es una opción para estructurarlo.

![Estructura de la carpeta de mods](images/EstructuraMods.png)

Para *Modern Industrialization* cree una carpeta por máquina a la que voy a agregar recetas porque van a existir muchas, dentro de las carpetas por máquina estoy creando un archivo por cada receta que estoy agregando.

Para *Create* solo cree un archivo por máquina porque el código de borrar la máquina y sus recetas es muy corto (2 líneas como mínimo).

NOTA: Si se está utilizando la carpeta por máquina cuando se agregan recetas hay que poner el archivo en la carpeta de la máquina en la cual se está agregando la receta (ej. *brass sheet* es una receta que solo está en create pero lo estoy metiendo en el compressor de MI), mientras que si es está eliminando o modificando el archivo debería estar en el máquina que tiene la receta originalmente.

# Recipes references

- For [Modern Industrialization](https://github.com/AztechMC/Modern-Industrialization/tree/master/src/main/resources/data/modern_industrialization/recipes) 
- [Tiers de herramientas](https://minecraft.fandom.com/wiki/Tiers)
- [Tiers de armaduras](https://minecraft.fandom.com/wiki/Armor_materials)